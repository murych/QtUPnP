#ifndef ARGUMENT_HPP
#define ARGUMENT_HPP

#include <QSharedDataPointer>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

/*! Forward CArgument data. */
struct SArgumentData;

/*! \brief The CArgument class holds information about an action argument.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CArgument
{
public:
  /*! Direction of the variable. */
  enum EDir
  {
    Unknown,  //!< Unknown direction.
    In,  //!< The value is sent.
    Out  //!< The value is returned.
  };

  /*! Defaut constructor. */
  CArgument();

  /*! Copy constructor. */
  CArgument(CArgument const& other);

  /*! Equal operator. */
  CArgument& operator=(CArgument const& other);
  ~CArgument();

  /*! Returns the direction of the argument. */
  [[nodiscard]] auto dir() const -> EDir;

  /*! Returns the related variable name. */
  [[nodiscard]] auto relatedStateVariable() const -> QString;

  /*! Sets the direction of the argument. */
  void setDir(EDir dir);

  /*! Sets the name of the related variable. */
  void setRelatedStateVariable(const QString& var);

private:
  QSharedDataPointer<SArgumentData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // ARGUMENT_HPP
