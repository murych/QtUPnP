#include "transportinfo.hpp"

namespace QtUPnP
{

/*! \brief Internal structure of CTransportInfo. */
struct STransportInfoData : public QSharedData
{
  STransportInfoData() = default;

  QString m_currentTransportState;
  QString m_currentTransportStatus;
  QString m_currentSpeed {"1"};
};

}  // namespace QtUPnP

using namespace QtUPnP;

CTransportInfo::CTransportInfo()
    : m_d {new STransportInfoData}
{
}

CTransportInfo::CTransportInfo(CTransportInfo const& rhs)
    : m_d {rhs.m_d}
{
}

CTransportInfo& CTransportInfo::operator=(CTransportInfo const& rhs)
{
  if (this != &rhs) {
    m_d.operator=(rhs.m_d);
  }

  return *this;
}

CTransportInfo::~CTransportInfo() = default;

void CTransportInfo::setCurrentTransportState(const QString& state)
{
  m_d->m_currentTransportState = state;
}

void CTransportInfo::setCurrentTransportStatus(const QString& status)
{
  m_d->m_currentTransportStatus = status;
}

void CTransportInfo::setCurrentSpeed(const QString& speed)
{
  m_d->m_currentSpeed = speed;
}

QString CTransportInfo::currentTransportState() const
{
  return m_d->m_currentTransportState;
}

QString CTransportInfo::currentTransportStatus() const
{
  return m_d->m_currentTransportStatus;
}

QString CTransportInfo::currentSpeed() const
{
  return m_d->m_currentSpeed;
}
