#ifndef INITIAL_DISCOVERY_HPP
#define INITIAL_DISCOVERY_HPP 1

#include <QEventLoop>
#include <QHostAddress>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

class CUpnpSocket;

/*! \brief Provides methods to discover the UPnP devices. */
class CInitialDiscovery : public QEventLoop
{
  Q_OBJECT

public:
  /*! Constructor.
   * \param socket: The socket to send discovery messages.
   * \param hostAddress: The ip address.
   * \param port: The port generally 1900.
   */
  CInitialDiscovery(CUpnpSocket* socket,
                    QHostAddress const& hostAddress,
                    uint16_t port);

  /*! Launchs discovery.
   * \param changeMX: True means the MX of the discovery message is changed at
   * each sended message. \param uuid: The destination uuid.
   */
  bool discover(bool changeMX = false, char const* uuid = nullptr);

  /*! Sets the discovery pause in milliseconds. */
  void setDiscoveryPause(int pause) { m_discoveryPause = pause; }

  /*! Restore the MX value at the default value (3s). */
  void restoreDefaultMX() { m_mx = 3; }

  /*! Sets the socket. */
  inline void setSocket(CUpnpSocket* socket);

  /*! Sets the ip address. */
  inline void setHostAddress(QHostAddress const& hostAddress);

  /*! Sets the port. */
  inline void setPort(uint16_t port);

  /*! Returns the port. */
  inline uint16_t port() const;

  /*! Returns the current MX value. */
  inline uint16_t mx() const;

protected:
  /*! Wrapper for timer event. */
  virtual void timerEvent(QTimerEvent* event);

protected:
  CUpnpSocket* m_socket;  //!< The socket.
  QHostAddress m_hostAddress;  //!< ip address.
  uint16_t m_port {0};  //!< The port.
  int16_t m_mx {5};  //!< The current MX value.
  int m_discoveryPause {
      0};  //!< Defualt discovery pause between each sent message (500ms).
  int m_idTimer;  //<! The id timer for timeout.
};

void CInitialDiscovery::setSocket(CUpnpSocket* socket)
{
  m_socket = socket;
}

void CInitialDiscovery::setHostAddress(QHostAddress const& hostAddress)
{
  m_hostAddress = hostAddress;
}

void CInitialDiscovery::setPort(uint16_t port)
{
  m_port = port;
}

uint16_t CInitialDiscovery::port() const
{
  return m_port;
}

uint16_t CInitialDiscovery::mx() const
{
  return m_mx;
}

}  // namespace QtUPnP

#endif  //_INITIAL_DISCOVERY_HPP
