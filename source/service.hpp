#ifndef SERVICE_HPP
#define SERVICE_HPP

#include <QMap>
#include <QSharedDataPointer>

#include "action.hpp"
#include "qtUPnP/qtUPnP_export.hpp"
#include "statevariable.hpp"

namespace QtUPnP
{

/*! The state variables map.
 * \param QString: The variable name.
 * \param CStateVariable: The state variable.
 */
using TMStateVariables = QMap<QString, CStateVariable>;

/*! The actions map.
 * \param QString: The variable name.
 * \param CAction: The action
 */
using TMActions = QMap<QString, CAction>;

/*! The forward declaration of CService data. */
struct SServiceData;

/*! \brief The CService class holds information about a service being advertised
 * or found by a control point.
 *
 * It maintains a list of actions and state variables.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CService
{
public:
  /*! Default constructor. */
  CService();

  /*! Copy constructor. */
  CService(CService const& other);

  /*! Equal operator. */
  CService& operator=(CService const& other);

  /*! Destructor. */
  ~CService();

  /*! Sets the service type. */
  void setServiceType(const QString& type);

  /*! Sets the control url. */
  void setControlURL(const QString&);

  /*! Sets the event suburl. */
  void setEventSubURL(const QString& url);

  /*! Sets the SCP url. */
  void setScpdURL(const QString& url);

  /*! Sets the Subsscribe SID. */
  void setSubscribeSID(const QString& id);

  /*! Sets the state variables. */
  void setStateVariables(const TMStateVariables& stateVariables);

  /*! Sets the actions. */
  void setActions(const TMActions& actions);

  /*! Sets the instance identifiers. */
  void setInstanceIDs(const QVector<unsigned>& ids);

  /*! Sets the major version. */
  void setMajorVersion(unsigned version);

  /*! Sets the minor version. */
  void setMinorVersion(unsigned version);

  /*! Sets the evented. A service is evented if at least one state variable is
   * evented. */
  void setEvented(bool evented);

  /*! Returns the service type. */
  [[nodiscard]] auto serviceType() const -> QString;

  /*! Returns the control url. */
  [[nodiscard]] auto controlURL() const -> QString;

  /*! Returns the event suburl. */
  [[nodiscard]] auto eventSubURL() const -> QString;

  /*! Returns the SCP url. */
  [[nodiscard]] auto scpdURL() const -> QString;

  /*! Returns the Subsscribe SID. */
  [[nodiscard]] auto subscribeSID() const -> QString;

  /*! Returns the state variables. */
  [[nodiscard]] auto stateVariables() const -> TMStateVariables const&;

  /*! Returns the state variables. */
  [[nodiscard]] auto stateVariables() -> TMStateVariables&;

  /*! Returns the actions. */
  [[nodiscard]] auto actions() const -> TMActions const&;

  /*! Sets the actions. */
  [[nodiscard]] auto actions() -> TMActions&;

  /*! Returns the instance identifiers. */
  [[nodiscard]] auto instanceIDs() const -> QVector<unsigned>;

  /*! Returns the major version. */
  [[nodiscard]] auto majorVersion() const -> unsigned;

  /*! Returns the minor version. */
  [[nodiscard]] auto minorVersion() const -> unsigned;

  /*! Returns rhe highest supported version of the device. */
  [[nodiscard]] auto highestSupportedVersion() const -> unsigned;

  /*! Returns the evented. A service is evented if at least one state variable
   * is evented. */
  [[nodiscard]] auto isEvented() const -> bool;

  /*! Clear the SID. */
  void clearSID();

  /*! Parse the xml data. */
  [[nodiscard]] auto parseXml(const QByteArray& data) -> bool;

  /*! Returns the state variable.
   * \param name: The variable name.
   * \return The variable.
   */
  [[nodiscard]] auto stateVariable(const QString& name) const -> CStateVariable;

private:
  QSharedDataPointer<SServiceData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // SERVICE_HPP
