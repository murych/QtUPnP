#ifndef XMLH_DEVICE_HPP
#define XMLH_DEVICE_HPP

#include <array>

#include "xmlh.hpp"

namespace QtUPnP
{

class CDevice;

/*! \brief Provides the implementation of the device response parser. */
class CXmlHDevice : public CXmlH
{
public:
  enum ETempService
  {
    Id,
    Type,
    ScpdURL,
    ControlURL,
    EventSubURL,
    Last
  };
  using TTempService = std::array<QString, Last>;

  /*! Default constructor. */
  CXmlHDevice(CDevice& device);

  ~CXmlHDevice() override = default;

  /*! The reader calls this function when it has parsed a start element tag.
   * See  QXmlContentHandler documentation.
   */
  auto startElement(const QString& namespaceURI,
                    const QString& localName,
                    const QString& qName,
                    const QXmlAttributes& atts) -> bool override;

  /*! The parser calls this function when it has parsed a chunk of character
   * data See  QXmlContentHandler documentation.
   */
  auto characters(const QString& name) -> bool override;

  /*! The reader calls this function when it has parsed an end element tag.
   * See  QXmlContentHandler documentation.
   */
  auto endElement(const QString& namespaceURI,
                  const QString& localName,
                  const QString& qName) -> bool override;

private:
  //! The device to updated.
  CDevice& m_device;

  //! The stack of services for the device and each embeded devices.
  QStack<QVector<TTempService>> m_tempServices;

  //! The stack of embeded devices.
  QStack<CDevice*> m_subDevices;

  //! Current parsed device.
  CDevice* m_current {nullptr};

  //! base url in case of embedded devices.
  QString m_urlBase;

  //! The xml contains a device list tag.
  bool m_deviceList {false};
};

}  // namespace QtUPnP

#endif  // XMLH_DEVICE_HPP
