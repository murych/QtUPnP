#include "transportsettings.hpp"

namespace QtUPnP
{

/*! \brief Internal structure of CTransportSettings. */
struct STransportSettingsData : public QSharedData
{
  STransportSettingsData() = default;

  QString m_playMode;
  QString m_recQualityMode;
};

}  // namespace QtUPnP

using namespace QtUPnP;

CTransportSettings::CTransportSettings()
    : m_d {new STransportSettingsData}
{
}

CTransportSettings::CTransportSettings(CTransportSettings const& rhs)
    : m_d {rhs.m_d}
{
}

CTransportSettings& CTransportSettings::operator=(CTransportSettings const& rhs)
{
  if (this != &rhs) {
    m_d.operator=(rhs.m_d);
  }

  return *this;
}

CTransportSettings::~CTransportSettings() = default;

void CTransportSettings::setPlayMode(const QString& mode)
{
  m_d->m_playMode = mode;
}

void CTransportSettings::setRecQualityMode(const QString& mode)
{
  m_d->m_recQualityMode = mode;
}

QString CTransportSettings::playMode() const
{
  return m_d->m_playMode;
}

QString CTransportSettings::recQualityMode() const
{
  return m_d->m_recQualityMode;
}
