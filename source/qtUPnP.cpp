#include <string>

#include "qtUPnP/qtUPnP.hpp"

exported_class::exported_class()
    : m_name {"qtUPnP"}
{
}

auto exported_class::name() const -> const char*
{
  return m_name.c_str();
}
