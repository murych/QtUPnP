#ifndef DEVICE_MAP_HPP
#define DEVICE_MAP_HPP 1

#include <QTimer>

#include "device.hpp"
#include "eventingmanager.hpp"
#include "qtUPnP/qtUPnP_export.hpp"
#include "upnpsocket.hpp"

class QNetworkAccessManager;

namespace QtUPnP
{

/*! Defines the map of devices. */
using TMDevices = QMap<QString, CDevice>;

class CHTTPServer;

/*! \brief It is the container of devices. */
class QTUPNP_EXPORT CDeviceMap : public TMDevices
{
public:
  /*! Default constructor. */
  CDeviceMap();

  /*! Destructor. */
  ~CDeviceMap();

  /*! Extracts devices from CUpnpSocket responds and add it in the map.
   * \param nDevices: upnp socket responds.
   * \param timeout: Timeout for image request.
   * \return The number of devices in the map.
   */
  auto extractDevicesFromNotify(const QList<CUpnpSocket::SNDevice>& nDevices,
                                int timeout = CDataCaller::Timeout) -> int;

  /*! Subscribes eventing services. */
  auto subscribe(CDevice& device,
                 int renewalDelay = CEventingManager::RenewalDelay,
                 int requestTimeout = CEventingManager::RequestTimeout) -> bool;

  /*! Unsubscribes eventing services. */
  void unsubscribe(CDevice& device,
                   int requestTimeout = CEventingManager::RequestTimeout);

  /*! Renews subscribe eventing services. */
  void renewSubscribe(CDevice& device,
                      int requestTimeout = CEventingManager::RequestTimeout);

  /*! Returns the uuid from id address.
   * \param host: The ip address.
   * \return The uuid.
   */
  [[nodiscard]] auto uuid(const QString& host) const -> QString;

  /*! Returns the device uuid and the service from event sid.
   * \param sid: The event sid.
   * \return first=device uuid, second=service id.
   */
  auto eventSender(const QString& sid) const -> QPair<QString, QString>;

  /*! Returns the tcp server for eventing. */
  [[nodiscard]] const auto httpServer() const { return m_httpServer; }

  /*! Removes a device of the map.
   *\param uuid: The device uuid.
   */
  void removeDevice(const QString& uuid);

  /*! Returns the new device list as a reference. */
  [[nodiscard]] auto newDevices() -> QStringList& { return m_newDevices; }

  /*! Returns the lost device list as a reference. */
  [[nodiscard]] auto lostDevices() -> QStringList& { return m_lostDevices; }

  /*! Returns the new device list as a const reference. */
  [[nodiscard]] auto newDevices() const -> const QStringList&
  {
    return m_newDevices;
  }

  /*! Returns the lost device list as a const reference. */
  [[nodiscard]] auto lostDevices() const -> const QStringList&
  {
    return m_lostDevices;
  }

  /*! Returns the netwok access manager created to speed up retreive data. */
  [[nodiscard]] const auto networkAccessManager() const { return m_naMgr; }

  /*! Set UPnP/AV only. Just AV servers and renderers are handle */
  void setAVOnly() { m_avOnly = true; }

  /*! Expand embedded devices. Embedded devices are also copy in the map and
   * accessible by CControlPoint::device (Qstring const & uuid).
   */
  void setExpandEmbeddedDevices() { m_expandEmbeddedDevices = true; }

protected:
  /*! Inserts a new device. */
  void insertDevice(CDevice& device);

  /*! Inserts an empty device.
   * \param uuid: The uuid of the device.
   * \return An iterator of the new device.
   */
  iterator insertDevice(const QString& uuid);

  /*! Extracts the services components. */
  auto extractServiceComponents(CDevice& device, int timeout) -> bool;

private:
  //! The http server for eventing.
  CHTTPServer* m_httpServer {nullptr};

  //! The network access manager for dataCaller.
  QNetworkAccessManager* m_naMgr {nullptr};

  //! List of new devices.
  QStringList m_newDevices;

  //! List of lost devices.
  QStringList m_lostDevices;

  //! Invalid devices. The device is invalid when get services fails twice.
  QMap<QString, int> m_invalidDevices;

  //! Max number of fails to consider the device invalid.
  int m_deviceFails {2};

  //! The discovery is launched from avDiscovery.
  bool m_avOnly {false};

  //! Embedded devices are also insert in the map.
  bool m_expandEmbeddedDevices {false};
};

}  // namespace QtUPnP

#endif
