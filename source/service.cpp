#include "service.hpp"

#include "xmlhservice.hpp"

namespace QtUPnP
{

/*! \brief Internal structure of CService. */
struct SServiceData : public QSharedData
{
  SServiceData() = default;

  QString m_serviceType;
  QString m_controlURL;
  QString m_eventSubURL;
  QString m_scpdURL;
  QString m_subscribeSID;
  TMStateVariables m_stateVariables;
  TMActions m_actions;
  QVector<unsigned> m_instanceIDs = {0};
  unsigned short m_minorVersion {0};
  unsigned short m_majorVersion {1};
  bool m_evented {false};
};

}  // namespace QtUPnP

using namespace QtUPnP;

CService::CService()
    : m_d {new SServiceData}
{
}

CService::CService(CService const& other)
    : m_d {other.m_d}
{
}

CService& CService::operator=(CService const& other)
{
  if (this != &other) {
    m_d.operator=(other.m_d);
  }

  return *this;
}

CService::~CService() = default;

void CService::setServiceType(const QString& type)
{
  m_d->m_serviceType = type;
}

void CService::setControlURL(const QString& url)
{
  m_d->m_controlURL = url;
}

void CService::setEventSubURL(const QString& url)
{
  m_d->m_eventSubURL = url;
}

void CService::setScpdURL(const QString& url)
{
  m_d->m_scpdURL = url;
}

void CService::setSubscribeSID(const QString& id)
{
  m_d->m_subscribeSID = id;
}

void CService::setInstanceIDs(const QVector<unsigned>& ids)
{
  m_d->m_instanceIDs = ids;
}

void CService::setStateVariables(const TMStateVariables& stateVariables)
{
  m_d->m_stateVariables = stateVariables;
}

void CService::setActions(const TMActions& actions)
{
  m_d->m_actions = actions;
}

void CService::setMajorVersion(unsigned version)
{
  m_d->m_majorVersion = static_cast<unsigned short>(version);
}

void CService::setMinorVersion(unsigned version)
{
  m_d->m_minorVersion = static_cast<unsigned short>(version);
}

void CService::setEvented(bool evented)
{
  m_d->m_evented = evented;
}

QString CService::serviceType() const
{
  return m_d->m_serviceType;
}

QString CService::controlURL() const
{
  return m_d->m_controlURL;
}

QString CService::eventSubURL() const
{
  return m_d->m_eventSubURL;
}

QString CService::scpdURL() const
{
  return m_d->m_scpdURL;
}

QString CService::subscribeSID() const
{
  return m_d->m_subscribeSID;
}

QVector<unsigned> CService::instanceIDs() const
{
  return m_d->m_instanceIDs;
}

TMStateVariables const& CService::stateVariables() const
{
  return m_d->m_stateVariables;
}

TMStateVariables& CService::stateVariables()
{
  return m_d->m_stateVariables;
}

TMActions const& CService::actions() const
{
  return m_d->m_actions;
}

TMActions& CService::actions()
{
  return m_d->m_actions;
}

unsigned CService::minorVersion() const
{
  return m_d->m_minorVersion;
}

unsigned CService::highestSupportedVersion() const
{
  unsigned version {0};
  int index {m_d->m_serviceType.lastIndexOf(':')};
  if (index != -1) {
    const auto s {
        m_d->m_serviceType.right(m_d->m_serviceType.length() - index - 1)};
    version = s.toUInt();
  }

  return version;
}

unsigned CService::majorVersion() const
{
  return m_d->m_majorVersion;
}

bool CService::isEvented() const
{
  return m_d->m_evented;
}

void CService::clearSID()
{
  m_d->m_subscribeSID.clear();
}

bool CService::parseXml(const QByteArray& data)
{
  CXmlHService h {*this};
  return h.parse(data) | CXmlH::tolerantMode();
}

CStateVariable CService::stateVariable(const QString& name) const
{
  return m_d->m_stateVariables.value(name);
}
