#include "statevariable.hpp"

namespace QtUPnP
{

/*! \brief Internal structure of CStateVariable. */
struct SStateVariableData : public QSharedData
{
  SStateVariableData() { m_values.push_back(TValue {}); }

  int m_type {0};
  bool m_evented {false};
  QStringList m_allowedValues;
  double m_minimum {0}, m_maximum {0}, m_step {0};
  QList<TValue> m_values;
};

}  // namespace QtUPnP

using namespace QtUPnP;

CStateVariable::CStateVariable()
    : m_d {new SStateVariableData}
{
}

CStateVariable::CStateVariable(CStateVariable const& other)
    : m_d {other.m_d}
{
}

CStateVariable& CStateVariable::operator=(CStateVariable const& other)
{
  if (this != &other) {
    m_d.operator=(other.m_d);
  }

  return *this;
}

CStateVariable::~CStateVariable() = default;

void CStateVariable::setType(EType type)
{
  m_d->m_type = type;
}

void CStateVariable::setType(const QString& stype)
{
  m_d->m_type = typeFromString(stype);
}

void CStateVariable::setEvented(bool evented)
{
  m_d->m_evented = evented;
}

void CStateVariable::setAllowedValues(const QStringList& allowed)
{
  m_d->m_allowedValues = allowed;
}

void CStateVariable::addAllowedValue(const QString& allowed)
{
  m_d->m_allowedValues << allowed;
}

void CStateVariable::setMinimum(double minimum)
{
  m_d->m_minimum = minimum;
}

void CStateVariable::setMaximum(double maximum)
{
  m_d->m_maximum = maximum;
}

void CStateVariable::setStep(double step)
{
  m_d->m_step = step;
}

bool CStateVariable::sameConstraints(const QList<TConstraint>& c1s,
                                     const QList<TConstraint>& c2s)
{
  auto same {c1s.size() == c2s.size()};
  if (same) {
    auto count {0};
    for (TConstraint const& c1 : c1s) {
      for (TConstraint const& c2 : c2s) {
        if (c1.first.compare(c2.first, Qt::CaseInsensitive) == 0
            && c1.second.compare(c2.second, Qt::CaseInsensitive) == 0)
        {
          ++count;
        }
      }
    }

    same = count == c1s.size();
  }

  return same;
}

QVariant CStateVariable::convert(const QString& value)
{
  auto ok {true};
  return [&]() -> QVariant
  {
    switch (static_cast<EType>(m_d->m_type)) {
      case I1: {
        const auto i1 {value.toShort(&ok)};
        return ok ? static_cast<int8_t>(i1) : QVariant {};
      }
      case Ui1: {
        const auto ui1 {value.toUShort(&ok)};
        return ok ? static_cast<uint8_t>(ui1) : QVariant {};
      }
      case I2: {
        const auto i2 {value.toShort(&ok)};
        return ok ? i2 : QVariant {};
      }
      case Ui2: {
        const auto ui2 {value.toUShort(&ok)};
        return ok ? ui2 : QVariant {};
      }
      case I4: {
        const auto i4 {value.toInt(&ok)};
        return ok ? i4 : QVariant {};
      }
      case Ui4: {
        const auto ui4 {value.toUInt(&ok)};
        return ok ? ui4 : QVariant {};
      }
      case I8: {
        const auto i8 {value.toLongLong(&ok)};
        return ok ? i8 : QVariant {};
      }
      case Ui8: {
        const auto ui8 {value.toULongLong(&ok)};
        return ok ? ui8 : QVariant {};
      }
      case Real: {
        const auto real {value.toDouble(&ok)};
        return ok ? real : QVariant {};
      }
      case Boolean: {
        if (value.isEmpty()) {
          return QVariant {};
        }
        const auto c {value.at(0).toLatin1()};
        return (c == '1' || c == 't' || c == 'T' || c == 't' || c == 'Y');
      }
      case String:
        return value;
      case Unknown:
        return QVariant {};
    }
  }();
}

void CStateVariable::setValue(const QString& value,
                              QList<TConstraint> const& constraints)
{
  auto update {false};
  for (auto& val : m_d->m_values) {
    const auto variableContraints {val.second};
    if (sameConstraints(variableContraints, constraints)) {
      update = true;
      val.first = convert(value);
      break;
    }
  }

  if (!update) {
    const auto variant {convert(value)};
    m_d->m_values.push_back({variant, constraints});
  }
}

void CStateVariable::setValue(const QString& value)
{
  m_d->m_values.first().first = convert(value);
}

void CStateVariable::setConstraints(const QList<TConstraint>& csts)
{
  m_d->m_values.first().second = csts;
}

CStateVariable::EType CStateVariable::type() const
{
  return static_cast<CStateVariable::EType>(m_d->m_type);
}

bool CStateVariable::isEvented() const
{
  return m_d->m_evented;
}

QStringList CStateVariable::allowedValues() const
{
  return m_d->m_allowedValues;
}

double CStateVariable::minimum() const
{
  return m_d->m_minimum;
}

double CStateVariable::maximum() const
{
  return m_d->m_maximum;
}

double CStateVariable::step() const
{
  return m_d->m_step;
}

QVariant CStateVariable::value(const QList<TConstraint>& constraints) const
{
  QVariant variant;
  for (const auto& val : m_d->m_values) {
    const auto variableContraints {val.second};
    if (sameConstraints(variableContraints, constraints)) {
      variant = val.first;
      break;
    }
  }

  return variant;
}

QVariant CStateVariable::value() const
{
  return m_d->m_values.first().first;  // At least one value exists.
}

QList<TConstraint>& CStateVariable::constraints()
{
  return m_d->m_values.first().second;  // At least one value exists.
}

QList<TConstraint> const& CStateVariable::constraints() const
{
  return m_d->m_values.first().second;  // At least one value exists.
}

bool CStateVariable::isValid() const
{
  return m_d->m_type != Unknown;
}

CStateVariable::EType CStateVariable::typeFromString(const QString& stype)
{
  if (stype == "string") {
    return CStateVariable::String;
  }
  if (stype == "i1") {
    return CStateVariable::I1;
  }
  if (stype == "ui1") {
    return CStateVariable::Ui1;
  }
  if (stype == "i2") {
    return CStateVariable::I2;
  }
  if (stype == "ui2") {
    return CStateVariable::Ui4;
  }
  if (stype == "i4") {
    return CStateVariable::I4;
  }
  if (stype == "ui4") {
    return CStateVariable::Ui4;
  }
  if (stype == "i8") {
    return CStateVariable::I8;
  }
  if (stype == "ui8") {
    return CStateVariable::Ui8;
  }
  if (stype == "real") {
    return CStateVariable::Real;
  }
  if (stype == "boolean") {
    return CStateVariable::Boolean;
  }

  return CStateVariable::Unknown;
}

QString CStateVariable::toString() const
{
  return m_d->m_values.first().first.toString();
}
