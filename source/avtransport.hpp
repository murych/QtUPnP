#ifndef AVTRANSPORT_HPP
#define AVTRANSPORT_HPP

#include "control.hpp"
#include "devicecaps.hpp"
#include "mediainfo.hpp"
#include "positioninfo.hpp"
#include "qtUPnP/qtUPnP_export.hpp"
#include "transportinfo.hpp"
#include "transportsettings.hpp"

namespace QtUPnP
{

class CControlPoint;
class CDidlItem;
class CActionInfo;

/*! \brief A wrapper that manages the urn:schemas-upnp-org:service:AVTransport:1
 * service actions.
 */
class QTUPNP_EXPORT CAVTransport : public CControl
{
public:
  /*! Default constructor. */
  CAVTransport() = default;

  /*! Constructor.
   * \param cp: The control point of the application.
   */
  explicit CAVTransport(CControlPoint* cp)
      : CControl {cp}
  {
  }

  /*! Invokes GetCurrenTransportActions.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return Current possible actions.
   */
  [[nodiscard]] QStringList getCurrentTransportActions(
      const QString& rendererUUID, unsigned instanceID = 0);

  /*! Invokes GetPositionInfo .
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return The current position info.
   */
  [[nodiscard]] CPositionInfo getPositionInfo(const QString& rendererUUID,
                                              unsigned instanceID = 0);

  /*! Invokes GetTransportSettings (NORMAL, REPEAT_ALL...)
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return The current transport settings.
   */
  [[nodiscard]] CTransportSettings getTransportSettings(
      const QString& rendererUUID, unsigned instanceID = 0);

  /*! Invokes GetDevicesCapabilities.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return The device capabilities.
   */
  [[nodiscard]] CDeviceCaps getDeviceCaps(const QString& rendererUUID,
                                          unsigned instanceID = 0);

  /*! Invokes GetTransportInfo.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return The transport info.
   */
  [[nodiscard]] CTransportInfo getTransportInfo(const QString& rendererUUID,
                                                unsigned instanceID = 0);

  /*! Invokes GetMediaInfo.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return The media info.
   */
  [[nodiscard]] CMediaInfo getMediaInfo(const QString& rendererUUID,
                                        unsigned instanceID = 0);

  /*! Invokes SetAVTransportURI.
   * \param rendererUUID: Renderer uuid.
   * \param item: The didl of the item to play (music track, movie...)
   * \param index: The index of the xml res tag.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool setAVTransportURI(const QString& rendererUUID,
                                       CDidlItem const& item,
                                       int index = 0,
                                       unsigned instanceID = 0);

  /*! Invokes SetAVTransportURI.
   * \details This function calls the "SetAVTransportURI" action with an address
   * that defines a playlist. The playlist is created from items. You can verify
   * if the renderer accepts playlists by calling the function
   * CDevice::canManagePlaylists.
   * For this function, the server is the HTTPSever of this library. It send the
   * formatted playlist to the renderer when resquested.
   * \param rendererUUID: Renderer uuid.
   * \param playlistName: The name of the playlist which will be displayed by
   * the renderer. \param items: The items to creates the playlist. \param
   * format: The playlist format. Actually only m3u and m3u8 are supported.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   * \note No playlist format verification is performed.
   */
  [[nodiscard]] bool setAVTransportURI(
      const QString& rendererUUID,
      const QString& playlistName,
      QList<CDidlItem::TPlaylistElem> const& items,
      CDidlItem::EPlaylistFormat format = CDidlItem::M3u,
      unsigned int instanceID = 0);

  /*! Invokes SetAVTransportURI.
   * \param rendererUUID: Renderer uuid.
   * \param uri: The uri to play.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   * \remark No file format verification is performed.
   */
  [[nodiscard]] bool setAVTransportURI(const QString& rendererUUID,
                                       const QString& uri,
                                       unsigned instanceID = 0);

  /*! Invokes SetNextAVTransportURI.
   * \param rendererUUID: Renderer uuid.
   * \param item: The CDidlItem.
   * \param index: The res element index.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   * \remark No file format verification is performed.
   */
  [[nodiscard]] bool setNextAVTransportURI(const QString& rendererUUID,
                                           CDidlItem const& item,
                                           int index = 0,
                                           unsigned instanceID = 0);

  /*! Invokes SetNextAVTransportURI.
   * \param rendererUUID: Renderer uuid.
   * \param uri: The uri to play.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   * \remark No file format verification is performed.
   */
  [[nodiscard]] bool setNextAVTransportURI(const QString& rendererUUID,
                                           const QString& uri,
                                           unsigned instanceID = 0);

  /*! Invokes SetPlayMode.
   * \param rendererUUID: Renderer uuid.
   * \param mode: The play mode (NORMAL, REPEAT_ALL, RANDOM, ...)
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   * \remark No file format verification is performed.
   */
  [[nodiscard]] bool setPlayMode(const QString& rendererUUID,
                                 const QString& mode,
                                 unsigned instanceID = 0);

  /*! Invokes Play.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool play(const QString& rendererUUID, unsigned instanceID = 0);

  /*! Invokes Pause.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool pause(const QString& rendererUUID,
                           unsigned instanceID = 0);

  /*! Invokes Stop.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool stop(const QString& rendererUUID, unsigned instanceID = 0);

  /*! Invokes SetPPS.
   * \param rendererUUID: Renderer uuid.
   * \param actionName: Action name (Play, Pause, Stop).
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool setPPS(const QString& rendererUUID,
                            const QString& actionName,
                            unsigned instanceID = 0);

  /*! Sends next.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool next(const QString& rendererUUID, unsigned instanceID = 0);

  /*! Sends previous.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \param rendererUUID: Renderer uuid.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool previous(const QString& rendererUUID,
                              unsigned instanceID = 0);

  /*! Sends seek action to time position.
   * \param rendererUUID: Renderer uuid.
   * \param timePosition: The position to go. If timePosition="Beginning", the
   * seeking position is 0:00:00.000.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool seek(const QString& rendererUUID,
                          const QString& timePosition,
                          unsigned instanceID = 0);

  /*! Sends seek action to track number.
   * \param rendererUUID: Renderer uuid.
   * \param iTrack: Track number start at 1.
   * \param instanceID: The value returned by GetCurrentConnectionIDs.
   * \return true in case of success. Otherwise false.
   */
  [[nodiscard]] bool seek(const QString& rendererUUID,
                          int iTrack,
                          unsigned instanceID = 0);

  /*! This code is to fix a problem detected with Cabasse Stream1.
   * If the Play action is sent to fast, the current track is good but the
   * renderer play always the track 1. To fix it, we wait 0.5s and get the
   * current track. If the current track is not good we retry the same code a
   * maximum of 5 times (wait max 2.5s).
   */
  [[nodiscard]] bool waitForAVTransportURI(const QString& renderer, int iTrack);
};

}  // namespace QtUPnP

#endif  // AVTRANSPORT_HPP
