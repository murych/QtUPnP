#ifndef CONNECTION_INFO_HPP
#define CONNECTION_INFO_HPP

#include <QSharedDataPointer>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

struct SConnectionInfoData;

/*! \brief The CConnectionInfo class holds information about
 * GetCurrentConnectionInfo action.
 *
 * See http://upnp.org/specs/av/UPnP-av-ConnectionManager-Service.pdf.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CConnectionInfo
{
public:
  /*! Default constructor. */
  CConnectionInfo();

  /*! Copy constructor. */
  CConnectionInfo(const CConnectionInfo&);

  /*! Equal operator. */
  CConnectionInfo& operator=(const CConnectionInfo&);

  /*! Destructor. */
  ~CConnectionInfo();

  /*! Sets the Rcs identifier. */
  void setRcsID(int id);

  /*! Sets the AVTransport identifier. */
  void setAVTransportID(int id);

  /*! Sets the protocol information. */
  void setProtocolInfo(const QString& protocol);

  /*! Sets the peer connection manager. */
  void setPeerConnectionManager(const QString& pcm);

  /*! Sets the peer connection identifier. */
  void setPeerConnectionID(int id);

  /*! Sets device the direction. */
  void setDirection(const QString& dir);

  /*! Sets device status. */
  void setStatus(const QString& st);

  /*! Returns the Rcs identifier. */
  [[nodiscard]] int rcsID() const;

  /*! Returns the AVTransport identifier. */
  [[nodiscard]] int avTransportID() const;

  /*! Returns the protocol information. */
  [[nodiscard]] QString protocolInfo() const;

  /*! Returns the peer connection manager. */
  [[nodiscard]] QString peerConnectionManager() const;

  /*! Returns the peer connection identifier. */
  [[nodiscard]] int peerConnectionID() const;

  /*! Returns device the direction. */
  [[nodiscard]] QString direction() const;

  /*! Returns device status. */
  [[nodiscard]] QString status() const;

private:
  QSharedDataPointer<SConnectionInfoData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // CONNECTION_INFO_HPP
