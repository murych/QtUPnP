#include "control.hpp"

#include "actioninfo.hpp"

using namespace QtUPnP;

QStringList CControl::fromCVS(QString entryString)
{
  QStringList list;
  if (!entryString.isEmpty()) {
    list = entryString.remove(' ').split(',');
  }

  return list;
}

bool CControl::toBool(const QString& entryString)
{
  bool value = false;
  if (!entryString.isEmpty()) {
    value =
        entryString[0] != '0' && entryString[0] != 'f' && entryString[0] != 'F';
  }

  return value;
}
