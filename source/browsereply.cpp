#include <QDebug>
#include <QRegularExpression>

#include "browsereply.hpp"

#include "helper.hpp"

namespace QtUPnP
{

/*! \brief Internal strucure for CBrowse. */
struct SBrowseReplyData : public QSharedData
{
  SBrowseReplyData() = default;

  uint m_numberReturned {0};
  uint m_totalMatches {0};
  uint m_updateID {0};
  QList<CDidlItem> m_items;
};

}  // Namespace

using namespace QtUPnP;

CBrowseReply::CBrowseReply()
    : m_d {new SBrowseReplyData}
{
}

CBrowseReply::CBrowseReply(CBrowseReply const& other)
    : m_d {other.m_d}
{
}

CBrowseReply& CBrowseReply::operator=(CBrowseReply const& other)
{
  if (this != &other) {
    m_d.operator=(other.m_d);
  }

  return *this;
}

CBrowseReply::~CBrowseReply() = default;

void CBrowseReply::setNumberReturned(unsigned numberReturned)
{
  m_d->m_numberReturned = numberReturned;
}

void CBrowseReply::setTotalMatches(unsigned totalMatches)
{
  m_d->m_totalMatches = totalMatches;
}

void CBrowseReply::setUpdateID(unsigned updateID)
{
  m_d->m_updateID = updateID;
}

void CBrowseReply::setItems(QList<CDidlItem> const& items)
{
  m_d->m_items = items;
}

unsigned CBrowseReply::numberReturned() const
{
  return m_d->m_numberReturned;
}

unsigned CBrowseReply::totalMatches() const
{
  return m_d->m_totalMatches;
}

unsigned CBrowseReply::updateID() const
{
  return m_d->m_updateID;
}

QList<CDidlItem> const& CBrowseReply::items() const
{
  return m_d->m_items;
}

QList<CDidlItem>& CBrowseReply::items()
{
  return m_d->m_items;
}

CBrowseReply& CBrowseReply::operator+=(CBrowseReply const& other)
{
  m_d->m_numberReturned += other.m_d->m_numberReturned;
  m_d->m_totalMatches += other.m_d->m_totalMatches;
  m_d->m_updateID = other.m_d->m_updateID;
  m_d->m_items += other.m_d->m_items;
  return *this;
}

QList<CDidlItem> CBrowseReply::search(QList<CDidlItem> const& items,
                                      QString text,
                                      int returned,
                                      int commonPrefixLength)
{
  using TDistance = QPair<float, int>;
  QList<CDidlItem> results;
  if (!text.isEmpty()) {
    const QRegularExpression re {"[\\s\\-,&°()':\\.\"]"};  // Separators.
    text = removeDiacritics(text.toUpper());

    // Update returned.
    auto cItems {items.size()};
    if (returned <= 0 || returned > items.size()) {
      returned = cItems;
    }

    const auto distanceMax {100000.0f};
    const auto texts {text.split(re, QString::SkipEmptyParts)};

    QVector<TDistance> distances(cItems);
    auto cMatches {0};
    QStringList titleComponents;
    titleComponents.reserve(20);

    QVector<QString> titles(cItems);  // To speed up pass 2.

    // Pass 1. Search exact matches. For each item title, the distance is
    // defined by the position in the title words and the index of the title
    // words. This defined the bigger distance by the position in the word and
    // the position int the list of words. Part 1 is to find a word every where
    // in the title.
    for (int iItem = 0; iItem < cItems; ++iItem) {
      auto biggerDistance {0.0f};
      titles[iItem] = removeDiacritics(items[iItem].title().toUpper());
      titleComponents = titles[iItem].split(re, QString::SkipEmptyParts);
      for (int k = 0, cTitleComponents = titleComponents.size();
           k < cTitleComponents;
           ++k)
      {
        for (const QString& text_ : texts) {
          const auto index {titleComponents.at(k).indexOf(text_)};
          if (index != -1) {
            const auto distance {distanceMax
                                 - static_cast<float>((index + 1) * (k + 1))};
            if (distance > biggerDistance) {
              biggerDistance = distance;
            }
          }
        }
      }

      if (!qFuzzyCompare(biggerDistance, 0.0f)) {
        distances.replace(iItem, {biggerDistance, iItem});
        ++cMatches;
      }
    }

    // Pass 2. Compute the Jaro-Winkler distance.
    if (cMatches < returned) {
      for (int iItem = 0; iItem < cItems; ++iItem) {
        if (qFuzzyCompare(distances.at(iItem).first, 0.0f)) {
          const auto distance {
              jaroWinklerDistance(text, titles[iItem], commonPrefixLength)};
          distances.replace(iItem, {distance, iItem});
        }
      }
    }

    std::sort(distances.begin(),
              distances.end(),
              [](const auto& d1, const auto& d2)
              { return d1.first > d2.first; });

    // Reorganized items.
    results.reserve(returned);
    for (int iItem = 0; iItem < returned; ++iItem) {
      const auto index {distances[iItem].second};
      results.push_back(items.at(index));
    }
  }

  return results;
}

QList<CDidlItem> CBrowseReply::search(const QString& text,
                                      int returned,
                                      int commonPrefixLength) const
{
  return search(m_d->m_items, text, returned, commonPrefixLength);
}

QStringList CBrowseReply::sortCapabilities(bool sameContent) const
{
  return sortCapabilities(*this, sameContent);
}

QStringList CBrowseReply::sortCapabilities(CBrowseReply const& reply,
                                           bool sameContent)
{
  QStringList caps;
  for (CDidlItem const& item : reply.m_d->m_items) {
    for (QMultiMap<QString, CDidlElem>::const_iterator
             ite = item.elems().cbegin(),
             ende = item.elems().cend();
         ite != ende;
         ++ite)
    {
      const QString& name = ite.key();
      if (!ite.value().isEmpty()) {
        QStringList::const_iterator end = caps.cend();
        if (std::find(caps.cbegin(), end, name) == end) {
          caps.push_back(name);
          TMProps const& props = (*ite).props();
          for (TMProps::const_iterator itp = props.cbegin(),
                                       endp = props.cend();
               itp != endp;
               ++itp)
          {
            if (!itp.value().isEmpty()) {
              caps.push_back(name + '@' + itp.key());
            }
          }
        }
      }
    }

    if (sameContent) {
      break;
    }
  }

  return caps;
}

void CBrowseReply::sort(CBrowseReply& reply,
                        const QString& criteria,
                        ESortDir dir)
{
  enum EComparType
  {
    String,
    Numerical,
    Time
  };

  QString elemName, propName;
  int index = criteria.indexOf('@');
  if (index != -1) {
    elemName = criteria.left(index);
    propName = criteria.right(criteria.length() - index - 1);
  } else {
    elemName = criteria;
  }

  auto type {String};
  QString value;
  QList<CDidlItem>& items = reply.m_d->m_items;
  for (CDidlItem const& item : items) {
    CDidlElem const& elem = item.value(elemName);
    value = index != -1 ? elem.props().value(propName) : elem.value();
    if (!value.isEmpty()) {
      bool ok;
      value.toLongLong(&ok);
      type = !ok && ::isDuration(value) ? Time : Numerical;
      break;
    }
  }

  if (!value.isEmpty()) {
    const auto kmax {15};
    QString value_;
    if (type != String) {
      value_.resize(kmax);
    }

    using TSortItem = QPair<QString, CDidlItem>;
    QVector<TSortItem> itemArray(items.size());
    for (int iItem = 0, end = items.size(); iItem < end; ++iItem) {
      const auto elem {items[iItem].value(elemName)};
      value_ = index != -1 ? elem.props().value(propName) : elem.value();
      if (type != String) {
        auto k {kmax - 1};
        for (int i = value_.length() - 1; i >= 0; --i, --k) {
          value_[k] = value_[i];
        }

        for (; k >= 0; --k) {
          value_[k] = '0';
        }
      }

      itemArray.replace(iItem, {value_, items.at(iItem)});
    }

    auto lt {[](TSortItem const& s1, TSortItem const& s2) -> bool
             { return s1.first < s2.first; }};
    auto gt {[](TSortItem const& s1, TSortItem const& s2) -> bool
             { return s1.first > s2.first; }};
    std::sort(itemArray.begin(), itemArray.end(), dir ? gt : lt);

    for (int iItem = 0, end = items.size(); iItem < end; ++iItem) {
      items.replace(iItem, itemArray.at(iItem).second);
    }
  }
}

void CBrowseReply::sort(const QString& criteria, ESortDir dir)
{
  return sort(*this, criteria, dir);
}

CBrowseReply operator+(CBrowseReply const& r1, CBrowseReply const& r2)
{
  CBrowseReply sum = r1;
  sum += r2;
  return sum;
}

QStringList CBrowseReply::dump()
{
  QStringList texts;
  texts << QString("numberReturned=%1,totalMatches=%2,updateID=%3")
               .arg(m_d->m_numberReturned)
               .arg(m_d->m_totalMatches)
               .arg(m_d->m_updateID);
  QList<CDidlItem> const& items = m_d->m_items;
  for (int iItem = 0; iItem < items.size(); ++iItem) {
    texts += QString("%1-----------------------").arg(iItem);
    texts += items[iItem].dump();
  }

  return texts;
}

void CBrowseReply::debug()
{
  QStringList texts = dump();
  for (const QString& text : texts) {
    qDebug() << text;
  }
}
