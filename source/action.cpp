#include "action.hpp"

namespace QtUPnP
{

/*! \brief Internal structure of CAction. */
struct SActionData : public QSharedData
{
  SActionData() = default;

  //! Map of arguments.
  TMArguments m_arguments;
};

CAction::CAction()
    : m_d {new SActionData}
{
}

CAction::CAction(CAction const& rhs)
    : m_d {rhs.m_d}
{
}

CAction& CAction::operator=(CAction const& other)
{
  if (this != &other) {
    m_d.operator=(other.m_d);
  }

  return *this;
}

CAction::~CAction() = default;

void CAction::setArguments(TMArguments const& args)
{
  m_d->m_arguments = args;
}

TMArguments CAction::arguments() const
{
  return m_d->m_arguments;
}

TMArguments& CAction::arguments()
{
  return m_d->m_arguments;
}

}  // namespace QtUPnP
