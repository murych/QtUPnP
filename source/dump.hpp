#ifndef DUMP_HPP
#define DUMP_HPP

#include <QObject>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

/*! \brief The CDump class provides just a signal with text data.
 * This class can be used to dump text information to a text widget (e.g.
 * QTextEdit). For this, just connect dumpReady signal to the user defined slot.
 * \code
 * QPlainTextEdit* debugInfo = new QPlainTextEdit (this);
 * connect (CDump::dumpObject (), SIGNAL(dumpReady(QString const &)), debugInfo,
 * SLOT(insertPlainText(QString const &))); \endcode
 */
class QTUPNP_EXPORT CDump : public QObject
{
  Q_OBJECT
public:
  /*! Contructor. */
  explicit CDump(QObject* parent = nullptr);

  ~CDump() override = default;

  /*! Returns the unique dump object. */
  static CDump* dumpObject() { return m_dump; }

  /*! Emits the signal from the unique object.
   * \param text: Data to emit.
   */
  static void dump(const QString& text);

  /*! Emits the signal from the unique object.
   * \param bytes: Data to emit.
   */
  static void dump(QByteArray const& bytes);

signals:
  /*! The signal. */
  void dumpReady(const QString& text);

private:
  static CDump* m_dump;  //!< The unique object.
};

}  // namespace QtUPnP

#endif  // DUMP_HPP
