#ifndef WAITING_LOOP_HPP
#define WAITING_LOOP_HPP

#include <QEventLoop>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

/*! \brief Defines a waiting loop to stop the current thread without
 * freeze the UI during certain operations.
 */
class QTUPNP_EXPORT CWaitingLoop : public QEventLoop
{
  Q_OBJECT

public:
  /*! Contructs CWaitingLoop with a delay of timeout. */
  CWaitingLoop(int timeout,
               ProcessEventsFlags flags,
               QObject* parent = nullptr);

  ~CWaitingLoop() override = default;

  /*! Starts the loop with a delay of timeout. */
  void start(int timeout) { m_idTimer = QEventLoop::startTimer(timeout); }

  /*! Creates and starts a waiting loop. */
  static void wait(int timeout, ProcessEventsFlags flags);

protected:
  /*! Event generate by the timer. */
  void timerEvent(QTimerEvent*) override;

private:
  ProcessEventsFlags m_flags {QEventLoop::AllEvents};

  //! Qt timer identifier. */
  int m_idTimer {0};
};

}  // namespace QtUPnP

#endif  // WAITING_LOOP_HPP
