#ifndef CONTENT_DIRECTORY_HPP
#define CONTENT_DIRECTORY_HPP

#include "browsereply.hpp"
#include "control.hpp"
#include "controlpoint.hpp"
#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

/*! \brief A wrapper that manages the
 * urn:schemas-upnp-org:service:ContentDirectory:1 service actions.
 */
class QTUPNP_EXPORT CContentDirectory : public CControl
{
public:
  /*! The browse types. */
  enum EBrowseType
  {
    BrowseMetaData,  //!< Return from metadata.
    BrowseDirectChildren,  //!< Return the children.
  };

  /*! The defaut constructor. */
  CContentDirectory() = default;

  /*! The constructor from a control point.
   * \param cp: The control point address.
   */
  explicit CContentDirectory(CControlPoint* cp)
      : CControl {cp}
  {
  }

  /*! Set the browse action tiemout.
   * \param timeout: The delay in ms. By default 20s.
   */
  void setBrowseTimeout(int timeout) { m_browseTimeout = timeout; }

  /*! Return the browse action timeout. */
  [[nodiscard]] auto browseTimeout() const { return m_browseTimeout; }

  /*! Browses from an identifier.
   * \param serverUUID: Server uuid.
   * \param objectID: Upnp identifier.
   * \param type: Type of browse. Must be BrowseMetaData or
   * BrowseDirectChildren. \param filter: Filter of the request. For syntax see
   * http://upnp.org/specs/av/UPnP-av-ContentDirectory-Service.pdf. The filter
   * capabilites is returned by GetSearchCapabilities action. \param
   * startingIndex: The first index returned. \param requestedCount: The max
   * number of resquests returned. 0 indicates all is rteurned. \param
   * sortCriteria: The sort criteria. For syntax see
   * http://upnp.org/specs/av/UPnP-av-ContentDirectory-Service.pdf. The sort
   * criteria is returned by GetSortCapabilities action. \return The decoded
   * result.
   */
  CBrowseReply browse(const QString& serverUUID,
                      const QString& objectID,
                      EBrowseType type = BrowseDirectChildren,
                      const QString& filter = "*",
                      int startingIndex = 0,
                      int requestedCount = 0,
                      const QString& sortCriteria = QString());

  /*! Searchs from a container identifier.
   * \param serverUUID: Server uuid.
   * \param containerID: Server uuid.
   * \param searchCriteria: Search criteria of the request. For syntax see
   * http://upnp.org/specs/av/UPnP-av-ContentDirectory-Service.pdf. The filter
   * capabilites is returned by GetSearchCapabilities action. \param filter:
   * Filter of the request. For syntax see
   * http://upnp.org/specs/av/UPnP-av-ContentDirectory-Service.pdf. The filter
   * capabilites is returned by GetSearchCapabilities action.* \param
   * startingIndex: The first index returned. \param requestedCount: The max
   * number of resquests returned. 0 indicates all is rteurned. \param
   * sortCriteria: The sort criteria. For syntax see
   * http://upnp.org/specs/av/UPnP-av-ContentDirectory-Service.pdf. The sort
   * criteria is returned by GetSortCapabilities action. \return The decoded
   * result.
   */
  CBrowseReply search(const QString& serverUUID,
                      const QString& containerID,
                      const QString& searchCriteria,
                      const QString& filter = "*",
                      int startingIndex = 0,
                      int requestedCount = 0,
                      const QString& sortCriteria = QString());

  /*! Returns search capabilities.
   * \param serverUUID: Server uuid.
   * \return The search capabilities.
   */
  QStringList getSearchCaps(const QString& serverUUID);

  /*! Returns sort capabilities.
   * Service urn:schemas-upnp-org:service:ContentDirectory.
   * \param serverUUID: Server uuid.
   * \return The sort capabilities.
   */
  QStringList getSortCaps(const QString& serverUUID);

  /*! Returns the system update ID.
   * \return Current system ID.
   */
  unsigned getSystemUpdateID(const QString& serverUUID);

  /*! Returns true if the item is valid.
   * \param item: CDidlItem to check.
   * \return True if item is valid.
   */
  bool isValidItem(CDidlItem const& item);

  /*! Returns the indices of invalid items.
   * \param items: CDidlItem to check.
   * \return The indices of invalid items.
   */
  QList<int> invalidItems(QList<CDidlItem> const& items);

private:
  /*! Prepares the arguments for browse action. See browse function for the
   * arguments. \return The list of parameters.
   */
  QList<CControlPoint::TArgValue> browseArguments(const QString& objectID,
                                                  EBrowseType type,
                                                  const QString& filter,
                                                  int startingIndex,
                                                  int requestedCount,
                                                  const QString& sortCriteria);

  /*! Prepares the arguments for search action. See search function for the
   * arguments. \return The list of parameters.
   */
  QList<CControlPoint::TArgValue> searchArguments(const QString& containerID,
                                                  const QString& searchCriteria,
                                                  const QString& filter,
                                                  int startingIndex,
                                                  int requestedCount,
                                                  const QString& sortCriteria);

private:
  //! Browse timeout to 20s.
  int m_browseTimeout {20000};

  QSet<QString> m_validatedItemKeys;
  QSet<QString> m_parentIDs;
};

}  // namespace QtUPnP

#endif  // CONTENT_DIRECTORY_HPP
