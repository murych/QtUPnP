#ifndef STATUS_HPP
#define STATUS_HPP

#include <cstdint>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

/*! \brief Holds information to simulate a set of 32 bits.
 *
 * It exists to simulate 2 states: exist or not.
 */
class QTUPNP_EXPORT CStatus
{
public:
  /*! Default constructor. */
  inline CStatus() = default;

  /*! Constructs with a st value. */
  inline explicit CStatus(unsigned st);

  /*! Copy constructor. */
  inline explicit CStatus(const CStatus& other);

  /*! Equal operator. */
  inline auto operator=(const CStatus& other) -> CStatus&;

  /*! Equal operator. */
  inline auto operator=(unsigned st) -> CStatus&;

  /*! Equality operator. */
  inline auto operator==(const CStatus& other) const -> bool;

  /*! Inequality operator. */
  inline auto operator!=(const CStatus& other) const -> bool;

  /*! Adds a set of bits. */
  inline void addStatus(unsigned st);

  /*! Sets a set of bits. */
  inline void setStatus(unsigned st);

  /*! Removes a set of bits. */
  inline void remStatus(unsigned st);

  /*! Has a set of bits. */
  inline auto hasStatus(unsigned st) const -> bool;

  /*! Return the set of bits. */
  inline auto status() const -> unsigned;

protected:
  uint32_t m_status {0};  //!< The 32 bits value of status.
};

CStatus::CStatus(unsigned st)
    : m_status {st}
{
}

CStatus::CStatus(CStatus const& other)
    : m_status {other.m_status}
{
}

CStatus& CStatus::operator=(const CStatus& other)
{
  m_status = other.m_status;
  return *this;
}

CStatus& CStatus::operator=(unsigned st)
{
  m_status = st;
  return *this;
}

void CStatus::addStatus(unsigned st)
{
  m_status |= st;
}

void CStatus::setStatus(unsigned st)
{
  m_status = st;
}

void CStatus::remStatus(unsigned st)
{
  m_status &= ~st;
}

bool CStatus::hasStatus(unsigned st) const
{
  return (m_status & st) != 0;
}

unsigned CStatus::status() const
{
  return m_status;
}

bool CStatus::operator==(const CStatus& other) const
{
  return m_status == other.m_status;
}

bool CStatus::operator!=(const CStatus& other) const
{
  return m_status != other.m_status;
}

/*! \brief Related non-members equal operator. */
inline bool operator==(const CStatus& s1, const CStatus& s2)
{
  return s1.status() == s2.status();
}

/*! \brief Related non-members not equal operator. */
inline bool operator!=(const CStatus& s1, const CStatus& s2)
{
  return s1.status() != s2.status();
}

}  // namespace QtUPnP

#endif  // STATUS_HPP
