#ifndef TRANSPORT_INFO_HPP
#define TRANSPORT_INFO_HPP

#include <QSharedDataPointer>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

struct STransportInfoData;

/*! \brief The CTransportInfo class holds information about GetTransportInfo
 * action.
 *
 * See http://upnp.org/specs/av/UPnP-av-AVTransport-Service.pdf.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CTransportInfo
{
public:
  /*! Default constructor. */
  CTransportInfo();

  /*! Copy constructor. */
  CTransportInfo(const CTransportInfo&);

  /*! Equal operator. */
  auto operator=(const CTransportInfo&) -> CTransportInfo&;

  /*! Destructor. */
  ~CTransportInfo();

  /*! Sets the current transport state. */
  void setCurrentTransportState(const QString& state);

  /*! Sets the current transport status. */
  void setCurrentTransportStatus(const QString& status);

  /*! Sets the current speed. */
  void setCurrentSpeed(const QString& speed);

  /*! Returns the current transport state. */
  [[nodiscard]] auto currentTransportState() const -> QString;

  /*! Returns the current transport status. */
  [[nodiscard]] auto currentTransportStatus() const -> QString;

  /*! Returns the current speed. */
  [[nodiscard]] auto currentSpeed() const -> QString;

private:
  QSharedDataPointer<STransportInfoData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // TRANSPORT_INFO_HPP
