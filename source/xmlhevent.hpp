#ifndef XMLH_EVENT_HPP
#define XMLH_EVENT_HPP

#include "httpserver.hpp"
#include "xmlh.hpp"

namespace QtUPnP
{

/*! \brief Provides the implementation of the event parser. */
class CXmlHEvent : public CXmlH
{
public:
  /*! Default constructor. */
  explicit CXmlHEvent(TMEventVars& vars);

  ~CXmlHEvent() override = default;

  /*! The reader calls this function when it has parsed a start element tag.
   * See  QXmlContentHandler documentation.
   */
  auto startElement(const QString& namespaceURI,
                    const QString& localName,
                    const QString& qName,
                    const QXmlAttributes& atts) -> bool override;

  /*! The parser calls this function when it has parsed a chunk of character
   * data See  QXmlContentHandler documentation.
   */
  auto characters(const QString& name) -> bool override;

  /*! The xml handler handler e:property tag (check=true) or just LastChange
   * evented variable (check=false). \internal
   */
  void setCheckProperty(bool check) { m_checkProperty = check; }

private:
  bool m_checkProperty {true};  //!< true=handle e:property tag, false=handle
                                //!< LastChange evented variable.
  TMEventVars& m_vars;  //!< The last modified ariable.
};

}  // namespace QtUPnP

#endif  // XMLH_EVENT_HPP
