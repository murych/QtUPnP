#include "contentdirectory.hpp"

#include "actioninfo.hpp"
#include "xmlhdidllite.hpp"

namespace QtUPnP
{

CBrowseReply CContentDirectory::browse(const QString& serverUUID,
                                       const QString& objectID,
                                       EBrowseType type,
                                       const QString& filter,
                                       int startingIndex,
                                       int requestedCount,
                                       const QString& sortCriteria)
{
  Q_ASSERT(m_cp != nullptr);
  CBrowseReply reply;
  int index = startingIndex, cReturned = 0;
  do {
    CBrowseReply tempReply;
    QList<CControlPoint::TArgValue> args = browseArguments(
        objectID, type, filter, index, requestedCount, sortCriteria);
    CActionInfo actionInfo =
        m_cp->invokeAction(serverUUID, "Browse", args, m_browseTimeout);
    if (actionInfo.succeeded()) {
      tempReply.setNumberReturned(args[7].second.toUInt());
      tempReply.setTotalMatches(args[8].second.toUInt());
      tempReply.setUpdateID(args[9].second.toUInt());
      cReturned = tempReply.numberReturned();
      if (cReturned != 0) {
        CXmlHDidlLite h;
        h.parse(args[6].second);
        tempReply.setItems(h.items());

        reply += tempReply;
        index += cReturned;
        requestedCount -= cReturned;
      } else {
        break;
      }
    } else {
      break;
    }
  } while (cReturned != 0 && requestedCount > 0);

  return reply;
}

CBrowseReply CContentDirectory::search(const QString& serverUUID,
                                       const QString& containerID,
                                       const QString& searchCriteria,
                                       const QString& filter,
                                       int startingIndex,
                                       int requestedCount,
                                       const QString& sortCriteria)
{
  Q_ASSERT(m_cp != nullptr);
  CBrowseReply reply;
  int index = startingIndex, cReturned = 0;
  do {
    CBrowseReply tempReply;
    QList<CControlPoint::TArgValue> args = searchArguments(containerID,
                                                           searchCriteria,
                                                           filter,
                                                           startingIndex,
                                                           requestedCount,
                                                           sortCriteria);
    CActionInfo actionInfo =
        m_cp->invokeAction(serverUUID, "Search", args, m_browseTimeout);
    if (actionInfo.succeeded()) {
      tempReply.setNumberReturned(args[7].second.toUInt());
      tempReply.setTotalMatches(args[8].second.toUInt());
      tempReply.setUpdateID(args[9].second.toUInt());
      cReturned = tempReply.numberReturned();
      if (cReturned != 0) {
        CXmlHDidlLite h;
        h.parse(args[6].second);
        tempReply.setItems(h.items());

        reply += tempReply;
        index += cReturned;
        requestedCount -= cReturned;
      } else {
        break;
      }
    } else {
      break;
    }
  } while (cReturned != 0 && requestedCount > 0);

  return reply;
}

QStringList CContentDirectory::getSearchCaps(const QString& serverUUID)
{
  Q_ASSERT(m_cp != nullptr);
  QStringList caps;
  QList<CControlPoint::TArgValue> args;
  args << CControlPoint::TArgValue("SearchCaps", {});
  CActionInfo actionInfo =
      m_cp->invokeAction(serverUUID, "GetSearchCapabilities", args);
  if (actionInfo.succeeded()) {
    caps = fromCVS(args.first().second);
  }

  return caps;
}

QStringList CContentDirectory::getSortCaps(const QString& serverUUID)
{
  Q_ASSERT(m_cp != nullptr);
  QStringList caps;
  QList<CControlPoint::TArgValue> args;
  args << CControlPoint::TArgValue("SortCaps", {});
  CActionInfo actionInfo =
      m_cp->invokeAction(serverUUID, "GetSortCapabilities", args);
  if (actionInfo.succeeded()) {
    caps = fromCVS(args.first().second);
  }

  return caps;
}

unsigned CContentDirectory::getSystemUpdateID(const QString& serverUUID)
{
  Q_ASSERT(m_cp != nullptr);
  unsigned id = 0;
  QList<CControlPoint::TArgValue> args;
  args << CControlPoint::TArgValue("Id", {});
  CActionInfo actionInfo =
      m_cp->invokeAction(serverUUID, "GetSystemUpdateID", args);
  if (actionInfo.succeeded()) {
    id = args.first().second.toUInt();
  }

  return id;
}

bool CContentDirectory::isValidItem(CDidlItem const& item)
{
  Q_ASSERT(m_cp != nullptr);
  auto key {[](const auto& item) -> QString
            { return item.uri(0) + item.itemID() + item.title(); }};

  auto itemKey {key(item)};
  auto identical {[&itemKey, key](const auto& item) -> bool
                  { return key(item) == itemKey; }};

  auto valid {m_validatedItemKeys.contains(itemKey)};
  if (!valid) {
    const auto server {m_cp->deviceUUID(item.uri(0), CDevice::MediaServer)};
    auto reply {
        browse(server, item.itemID(), CContentDirectory::BrowseMetaData)};
    if (reply.numberReturned() == 1) {
      const auto replyItems {reply.items()};
      if (replyItems.size() == 1) {
        const auto replyItem {replyItems.first()};
        if (identical(replyItem)) {
          valid = true;
          auto parentID {replyItem.parentID()};
          if (!m_parentIDs.contains(parentID)) {
            m_parentIDs.insert(parentID);
            reply = browse(server, parentID);
            std::for_each(replyItems.cbegin(),
                          replyItems.cend(),
                          [this, key](const auto& parentItem)
                          { m_validatedItemKeys.insert(key(parentItem)); });
          }
        }
      }
    }
  }

  return valid;
}

QList<int> CContentDirectory::invalidItems(QList<CDidlItem> const& items)
{
  Q_ASSERT(m_cp != nullptr);
  QList<int> indices;
  indices.reserve(items.size());
  auto index {0};
  for (const auto& item : qAsConst(items)) {
    if (!isValidItem(item)) {
      indices.push_back(index);
    }

    ++index;
  }

  return indices;
}

// -------------------------- Private code -----------------------------------
QList<CControlPoint::TArgValue> CContentDirectory::browseArguments(
    const QString& objectID,
    EBrowseType type,
    const QString& filter,
    int startingIndex,
    int requestedCount,
    const QString& sortCriteria)
{
  char const* browseFlag =
      type == BrowseDirectChildren ? "BrowseDirectChildren" : "BrowseMetadata";
  QList<CControlPoint::TArgValue> args;
  args.reserve(10);
  args << CControlPoint::TArgValue("ObjectID", objectID);
  args << CControlPoint::TArgValue("BrowseFlag", browseFlag);
  args << CControlPoint::TArgValue("Filter", filter);
  args << CControlPoint::TArgValue("StartingIndex",
                                   QString::number(startingIndex));
  args << CControlPoint::TArgValue("RequestedCount",
                                   QString::number(requestedCount));
  args << CControlPoint::TArgValue("SortCriteria", sortCriteria);
  args << CControlPoint::TArgValue("Result", {});
  args << CControlPoint::TArgValue("NumberReturned", {});
  args << CControlPoint::TArgValue("TotalMatches", {});
  args << CControlPoint::TArgValue("UpdateID", {});
  return args;
}

QList<CControlPoint::TArgValue> CContentDirectory::searchArguments(
    const QString& containerID,
    const QString& searchCriteria,
    const QString& filter,
    int startingIndex,
    int requestedCount,
    const QString& sortCriteria)
{
  QList<CControlPoint::TArgValue> args;
  args.reserve(10);
  args << CControlPoint::TArgValue("ContainerID", containerID);
  args << CControlPoint::TArgValue("SearchCriteria", searchCriteria);
  args << CControlPoint::TArgValue("Filter", filter);
  args << CControlPoint::TArgValue("StartingIndex",
                                   QString::number(startingIndex));
  args << CControlPoint::TArgValue("RequestedCount",
                                   QString::number(requestedCount));
  args << CControlPoint::TArgValue("SortCriteria", sortCriteria);
  args << CControlPoint::TArgValue("Result", {});
  args << CControlPoint::TArgValue("NumberReturned", {});
  args << CControlPoint::TArgValue("TotalMatches", {});
  args << CControlPoint::TArgValue("UpdateID", {});
  return args;
}

}  // namespace QtUPnP
