#include "argument.hpp"

namespace QtUPnP
{
/*! \brief Internal structure of CArgument. */
struct SArgumentData : public QSharedData
{
  SArgumentData() = default;
  SArgumentData(SArgumentData const& rhs);

  int m_dir {0};  //!< Argument direction.
  QString m_relatedStateVariable;  //!< The related variable name.
};

SArgumentData::SArgumentData(SArgumentData const& rhs)
    : QSharedData {rhs}
    , m_dir {rhs.m_dir}
    , m_relatedStateVariable {rhs.m_relatedStateVariable}
{
}

CArgument::CArgument()
    : m_d {new SArgumentData}
{
}

CArgument::CArgument(const CArgument& other)
    : m_d {other.m_d}
{
}

CArgument& CArgument::operator=(CArgument const& other)
{
  if (this != &other) {
    m_d.operator=(other.m_d);
  }

  return *this;
}

CArgument::~CArgument() {}

CArgument::EDir CArgument::dir() const
{
  return static_cast<EDir>(m_d->m_dir);
}

QString CArgument::relatedStateVariable() const
{
  return m_d->m_relatedStateVariable;
}

void CArgument::setDir(EDir dir)
{
  m_d->m_dir = dir;
}

void CArgument::setRelatedStateVariable(const QString& var)
{
  m_d->m_relatedStateVariable = var;
}

}  // namespace QtUPnP
