#ifndef STATE_VARIABLE_HPP
#define STATE_VARIABLE_HPP

#include <QSharedDataPointer>
#include <QVariant>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

struct SStateVariableData;

/*! Defines the constraint type.
 * \param QString: Constraint name.
 * \param QString: Constraint value.
 */
using TConstraint = QPair<QString, QString>;

/*! Defines variable value and constraint type.
 * \param QVariant: Variable value.
 * \param QList<TConstraint>: Constraint list.
 */
using TValue = QPair<QVariant, QList<TConstraint>>;

/*! \brief The CStateVariable class holds information about an UPnP state
 * variable.
 *
 * The state variable values are updated by action and UPnP events.
 * The values may not be the values in the device.
 * More than one values can exists.
 * The constraints are updated only by UPnP events. e.g. For the volume, the
 * UPnP event sends the constraint Channel=Master and the value.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CStateVariable
{
public:
  /*! Variable type. */
  enum EType
  {
    Unknown,  //!< Unknwon
    I1,  //!< 8  bits integer
    Ui1,  //!< 8 bits unsigned integer
    I2,  //!< 16  bits integer
    Ui2,  //!< 16 bits unsigned integer
    I4,  //!< 32 bits integer
    Ui4,  //!< 32 bits unsigned integer
    I8,  //!< 64 bits integer
    Ui8,  //!< 64 bits unsigned integer
    Real,  //!< double
    String,  //!< QString
    Boolean,  //!< bool
  };

  /*! Default constructor. */
  CStateVariable();

  /*! Copy constructor. */
  CStateVariable(CStateVariable const& other);

  /*! Equal operator. */
  auto operator=(CStateVariable const& other) -> CStateVariable&;

  /*! Destructor. */
  ~CStateVariable();

  /*! Sets the variable type. */
  void setType(EType type);

  /*! Sets the variable type from QString. */
  void setType(const QString& stype);

  /*! Sets the variable is evented. */
  void setEvented(bool evented);

  /*! Sets the allowed values. */
  void setAllowedValues(const QStringList& allowed);

  /*! Adds an allowed value. */
  void addAllowedValue(const QString& allowed);

  /*! Sets the value minimum. */
  void setMinimum(double minimum);

  /*! Sets the value maximum. */
  void setMaximum(double maximum);

  /*! Sets the variable step. */
  void setStep(double step);

  /*! Sets the value, always the first value. */
  void setValue(const QString& value);

  /*! Sets the value and constraints. */
  void setValue(const QString& value, const QList<TConstraint>& constraints);

  /*! Sets the variable constraints, always the first value. */
  void setConstraints(const QList<TConstraint>& csts);

  /*! Returns the variable type. */
  [[nodiscard]] auto type() const -> EType;

  /*! Returns the variable is evented. */
  [[nodiscard]] auto isEvented() const -> bool;

  /*! Returns the allowed values. */
  [[nodiscard]] auto allowedValues() const -> QStringList;

  /*! Returns the value minimum. */
  [[nodiscard]] auto minimum() const -> double;

  /*! Returns the value maximum. */
  [[nodiscard]] auto maximum() const -> double;

  /*! Returns the variable step. */
  [[nodiscard]] auto step() const -> double;

  /*! Returns the first value. */
  [[nodiscard]] auto value() const -> QVariant;

  /*! Returns the value for a list of contraints. */
  [[nodiscard]] auto value(const QList<TConstraint>& constraints) const
      -> QVariant;

  /*! Returns the variable first constraints as a const reference. */
  QList<TConstraint> const& constraints() const;

  /*! Returns the variable first constraints as a reference. */
  QList<TConstraint>& constraints();

  /*! Returns true if the variable has not the type Unknown. */
  bool isValid() const;

  /*! Return the first value as a QString. */
  QString toString() const;

  /*! Returns the type from QString. */
  static EType typeFromString(const QString& stype);

  /*! Returns the QVariant corresponding at value from the type. */
  QVariant convert(const QString& value);

  /*! Returns true if the 2 arguments are equal. The constraint names and values
   * are identical. */
  static bool sameConstraints(const QList<TConstraint>& c1s,
                              const QList<TConstraint>& c2s);

private:
  QSharedDataPointer<SStateVariableData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // STATE_VARIABLE_HPP
