#ifndef XMLH_SERVICE_HPP
#define XMLH_SERVICE_HPP

#include "action.hpp"
#include "statevariable.hpp"
#include "xmlh.hpp"

namespace QtUPnP
{

class CService;

/*! \brief Provides the implementation of the service response parser. */
class CXmlHService : public CXmlH
{
public:
  /*! Default constructor. */
  explicit CXmlHService(CService& service);

  ~CXmlHService() override = default;

  /*! The reader calls this function when it has parsed a start element tag.
   * See  QXmlContentHandler documentation.
   */
  auto startElement(const QString& namespaceURI,
                    const QString& localName,
                    const QString& qName,
                    const QXmlAttributes& atts) -> bool override;

  /*! The parser calls this function when it has parsed a chunk of character
   * data See  QXmlContentHandler documentation.
   */
  auto characters(const QString& name) -> bool override;

  /*! The reader calls this function when it has parsed a end element tag.
   * See  QXmlContentHandler documentation.
   */
  auto endElement(const QString& namespaceURI,
                  const QString& localName,
                  const QString& qName) -> bool override;

private:
  //! The couple name-state variable.
  std::pair<QString, CStateVariable> m_var;

  //! The couple name-action.
  std::pair<QString, CAction> m_action;

  //! The couple name-argument.
  std::pair<QString, CArgument> m_arg;

  //! The service to update.
  CService& m_service;
};

}  // namespace QtUPnP

#endif  // XMLH_SERVICE_HPP
