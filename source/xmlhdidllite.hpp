#ifndef XMLH_DIDL_LITE_HPP
#define XMLH_DIDL_LITE_HPP

#include "didlitem.hpp"
#include "xmlh.hpp"

namespace QtUPnP
{

/*! \brief Provides the implementation of the DIDL-Lite server response parser.
 * At the end of the parsing, this class contains the item list and their
 * elements.
 */
class CXmlHDidlLite : public CXmlH
{
public:
  /*! Default constructor. */
  CXmlHDidlLite();

  ~CXmlHDidlLite() override = default;

  /*! The reader calls this function when it has parsed a start element tag.
   * See  QXmlContentHandler documentation.
   */
  auto startElement(const QString& namespaceURI,
                    const QString& localName,
                    const QString& qName,
                    const QXmlAttributes& atts) -> bool override;

  /*! The parser calls this function when it has parsed a chunk of character
   * data See  QXmlContentHandler documentation.
   */
  auto characters(const QString& name) -> bool override;

  /*! Returns the lists of item components as a constant reference. */
  auto items() const { return m_items; }

  /*! Returns the first item of the item list. */
  auto firstItem(const QString& didlLite) -> CDidlItem;

  /*! Returns the first item of the item list. */
  auto firstItem(QByteArray const& data) -> CDidlItem;

private:
  QList<CDidlItem> m_items;  //!< item list. See CDidlIem.
};

}  // namespace QtUPnP

#endif  // XMLH_DIDL_LITE_HPP
