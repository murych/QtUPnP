#ifndef DEVICE_PIXMAP_HPP
#define DEVICE_PIXMAP_HPP

#include <QSharedDataPointer>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

struct SDevicePixmapData;

/*! \brief The CDevicePixmap class holds information about a device image.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CDevicePixmap
{
public:
  /*! Default constructor. */
  CDevicePixmap();

  /*! Copy constructor. */
  CDevicePixmap(const CDevicePixmap& other);

  /*! Equal operator. */
  CDevicePixmap& operator=(CDevicePixmap const& other);

  /*! Destructor. */
  ~CDevicePixmap();

  /*! Sets the image url. */
  void setUrl(const QString& url);

  /*! Sets the mime type. */
  void setMimeType(QString mimeType);

  /*! Sets the width. */
  void setWidth(int w);

  /*! Sets the height. */
  void setHeight(int h);

  /*! Sets the depth. */
  void setDepth(int d);

  /*! Returns the image url. */
  const QString& url() const;

  /*! Returns the mime type. */
  const QString& mimeType() const;

  /*! Returns the width. */
  int width() const;

  /*! Returns the height. */
  int height() const;

  /*! Returns the depth. */
  int depth() const;

  /*! Return the prefered criteria. It is width * height * depth. */
  int preferedCriteria() const;

  /*! Returns true if the image has the mimeTpe. */
  bool hasMimeType(char const* mimeType) const;

  /*! Return the prefered criteria. It is width * height * depth. */
  static int preferedCriteria(int w, int h, int d) { return w * h * d; }

private:
  QSharedDataPointer<SDevicePixmapData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // DEVICE_PIXMAP_HPP
