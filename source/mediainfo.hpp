#ifndef MEDIA_INFO_HPP
#define MEDIA_INFO_HPP

#include <QSharedDataPointer>

#include "didlitem.hpp"
#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

struct SMediaInfoData;

/*! \brief The CMediaInfo class holds information about GetMediaInfo action.
 *
 * See http://upnp.org/specs/av/UPnP-av-AVTransport-Service.pdf.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CMediaInfo
{
public:
  /*! Default constructor. */
  CMediaInfo();

  /*! Copy constructor. */
  CMediaInfo(const CMediaInfo&);

  /*! Equal operator. */
  CMediaInfo& operator=(const CMediaInfo&);

  /*! Destructor. */
  ~CMediaInfo();

  /*! Sets the track number.*/
  void setNrTracks(unsigned nrTracks);

  /*! Sets the media duration. */
  void setMediaDuration(const QString& duration);

  /*! Sets current uri. */
  void setCurrentURI(const QString& uri);

  /*! Sets current uri metadata. */
  void setCurrentURIMetaData(const QString& metadata);

  /*! Sets next uri. */
  void setNextURI(const QString& uri);

  /*! Sets next uri metadata. */
  void setNextURIMetaData(const QString& metadata);

  /*! Sets the play medium. */
  void setPlayMedium(const QString& medium);

  /*! Sets the record medium. */
  void setRecordMedium(const QString& medium);

  /*! Sets the write status. */
  void setWriteStatus(const QString& status);

  /*! Returns the track number.*/
  unsigned nrTracks() const;

  /*! Returns the media duration. */
  const QString& mediaDuration() const;

  /*! Returns current uri. */
  const QString& currentURI() const;

  /*! Returns current uri metadata. */
  const QString& currentURIMetaData() const;

  /*! Returns next uri. */
  const QString& nextURI() const;

  /*! Returns next uri metadata. */
  const QString& nextURIMetaData() const;

  /*! Returns the play medium. */
  const QString& playMedium() const;

  /*! Returns the record medium. */
  const QString& recordMedium() const;

  /*! Returns the write status. */
  const QString& writeStatus() const;

  /*! Converts the current uri metadata in a CDidlItem. */
  CDidlItem currentURIDidlItem() const;

  /*! Converts the next uri metadata in a CDidlItem. */
  CDidlItem nextURIDidlItem() const;

private:
  /*! Converts metadata in a CDidlItem. */
  static CDidlItem didlItem(const QString& metaData);

private:
  QSharedDataPointer<SMediaInfoData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // MEDIA_INFO_HPP
