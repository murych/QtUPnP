#ifndef XMLH_HPP
#define XMLH_HPP

#include <QStack>
#include <QXmlDefaultHandler>

namespace QtUPnP
{

/*! \brief The base class of XML parsers. */
class CXmlH : public QXmlDefaultHandler
{
public:
  /*! Default constructor. */
  CXmlH();

  /*! Destructor. */
  ~CXmlH() override;

  /*! Parse the XML response. */
  virtual bool parse(QByteArray const& response);

  /*! Parse the XML response. */
  virtual bool parse(const QString& response);

  /*! The reader calls this function when it has parsed a start element tag.
   * See  QXmlContentHandler documentation.
   */
  virtual bool startElement(const QString&,
                            const QString&,
                            const QString& qName,
                            QXmlAttributes const&) override;

  /*! The reader calls this function when it has parsed a end element tag.
   * See  QXmlContentHandler documentation.
   */
  virtual bool endElement(const QString&,
                          const QString&,
                          const QString&) override;

  /*! Replaces isolated char '&' by "&amp;".
   * Some renderer, use isolated char '&'. This stop the parser Qt xml parser.
   * For example, in the example below "&id=5786" stop the parser.
   * \code
   * <res
   * protocolInfo="http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_TN">http://awox.vtuner.com/dynamOD.asp?ex45v=6eaada6e4bcdb71ec2fa37e1bcf14068&id=5786</res>
   * \endcode
   * \param data: Uncoded string.
   * \return Coded string.
   */
  static QString ampersandHandler(const QString& data);

  /*! Sets the tolerant mode.
   * \param mode: True the parser always return true after parsing.
   * False, the parser stop when it detects an error in the xml.
   */
  static void setTolerantMode(bool mode) { m_tolerantMode = mode; }

  /*! Returns the parsing tolerant mode.
   * \see setTolerantMode.
   */
  static auto tolerantMode() { return m_tolerantMode; }

  /*! Sets the file name to dump xml errors.
   * By default the dump file name is empty and no dump is generated. *.
   */
  static void setDumpErrorFileName(const QString& fileName)
  {
    m_dumpErrorFileName = fileName;
  }

protected:
  /*! Returns the current tag at a level. */
  [[nodiscard]] auto tag(int level) const -> QString;

  /*! Returns the parent tag. */
  [[nodiscard]] auto tagParent() const { return tag(1); }

  /*! Returns a string start by character '/'.
   * e.g. name="connection/xml" returns "/connection/xml".
   * if name begin by '/', this function make nothing.
   */
  static auto prependSlash(const QString& name) -> QString;

  /*! Returns a string without name space specification.
   * e.g. name="s:connection" returns "connection"
   */
  static auto removeNameSpace(const QString& name) -> QString;

protected:
  //! Stack of tags.
  QStack<QString> m_stack;

  //! Defined the parsing mode.
  static bool m_tolerantMode;

  //! The file to dump xml errors (Can be empty).
  static QString m_dumpErrorFileName;
};

}  // namespace QtUPnP

#endif  // XMLH_HPP
