#include "xmlhevent.hpp"

using namespace QtUPnP;

CXmlHEvent::CXmlHEvent(TMEventVars& vars)
    : m_vars {vars}
{
}

bool CXmlHEvent::startElement(const QString& namespaceURI,
                              const QString& localName,
                              const QString& qName,
                              const QXmlAttributes& atts)
{
  CXmlH::startElement(namespaceURI, localName, qName, atts);
  auto tagParent {this->tagParent()};
  if (!m_checkProperty || tagParent == "e:property") {
    TEventValue val;
    for (int iAtt = 0, cAtts = atts.count(); iAtt < cAtts; ++iAtt) {
      auto name {atts.qName(iAtt)};
      auto value {atts.value(iAtt)};
      if (name == "val") {
        val.first = value;
      } else {
        val.second.push_back(TEventCst(name, value));
      }
    }

    if (qName == "Event") {
      val.first = namespaceURI;
    }

    auto name {removeNameSpace(qName)};
    m_vars.insert(name, val);
  }

  return true;
}

bool CXmlHEvent::characters(const QString& name)
{
  auto tag {m_stack.top()};
  if (tagParent() == "e:property") {
    TEventValue value;
    value.first = name;
    m_vars[removeNameSpace(tag)] = value;
    //    QMultiMap<QString, int>::iterator i = map.find("plenty");
    //    while (i != map.end() && i.key() == "plenty") {
    //      cout << i.value() << Qt::endl;
    //      ++i;
    //    }
    //    QMultiMap::iterator iter{m_vars.find("plenty")}
  }

  return true;
}
