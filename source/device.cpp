#include "device.hpp"

#include "datacaller.hpp"
#include "xmlhdevice.hpp"

namespace QtUPnP
{

/*! \brief Internal structure of CDevice. */
struct SDeviceData : public QSharedData
{
  SDeviceData() {}
  //  SDeviceData(SDeviceData const& other);

  mutable int8_t m_managePlaylists {
      -1};  //!< The renderer can managed playlits.
  int m_type {0};  //!< The type of the device e.g. server.
  QUrl m_url;  //!< The url.
  QString m_uuid;  //! < The uuid.
  QString m_modelName;  //!< The model name.
  QString m_modelNumber;  //!< The model number.
  QString m_modelURL;
  QString m_modelDesc;
  QString m_serialNumber;
  QString m_upc;
  QString m_presentationURL;
  QString m_manufacturer;  //!< Manufacturer.
  QString m_manufacturerURL;
  QString m_friendlyName;  //!< The friendly name.
  QString m_deviceType;  //!< Device type.
  QStringList m_dlnaDocs;
  QStringList m_dlnaCaps;
  QList<CDevicePixmap> m_pixmaps;  //!< The list of pixmaps.
  unsigned short m_minorVersion = 0;  //!< Minor version.
  unsigned short m_majorVersion = 1;  //!< Major version.
  unsigned m_connectionID = 0;
  TMServices m_services;  //!< The list of services.
  QList<CDevice> m_subDevices;  //!< Sub device list.
  QString m_parentUUID;  //!< Parent uuid in case of embedded device.
};

// SDeviceData::SDeviceData(SDeviceData const& other)
//     : QSharedData(other)
//     , m_managePlaylists(other.m_managePlaylists)
//     , m_type(other.m_type)
//     , m_url(other.m_url)
//     , m_uuid(other.m_uuid)
//     , m_modelName(other.m_modelName)
//     , m_modelNumber(other.m_modelNumber)
//     , m_modelURL(other.m_modelURL)
//     , m_modelDesc(other.m_modelDesc)
//     , m_serialNumber(other.m_serialNumber)
//     , m_manufacturer(other.m_manufacturer)
//     , m_manufacturerURL(other.m_manufacturerURL)
//     , m_friendlyName(other.m_friendlyName)
//     , m_deviceType(other.m_deviceType)
//     , m_dlnaDocs(other.m_dlnaDocs)
//     , m_dlnaCaps(other.m_dlnaCaps)
//     , m_pixmaps(other.m_pixmaps)
//     , m_minorVersion(other.m_minorVersion)
//     , m_majorVersion(other.m_majorVersion)
//     , m_connectionID(other.m_connectionID)
//     , m_services(other.m_services)
//     , m_subDevices(other.m_subDevices)
//     , m_parentUUID(other.m_parentUUID)
//{
// }

}  // namespace QtUPnP

using namespace QtUPnP;

CDevice::CDevice()
    : m_d {new SDeviceData}
{
}

CDevice::CDevice(CDevice const& rhs)
    : m_d {rhs.m_d}
{
}

CDevice& CDevice::operator=(CDevice const& rhs)
{
  if (this != &rhs) {
    m_d.operator=(rhs.m_d);
  }

  return *this;
}

CDevice::~CDevice() = default;

void CDevice::setType(EType type)
{
  m_d->m_type = type;
}

void CDevice::setType()
{
  const auto deviceType {this->deviceType()};
  const auto type {[deviceType]() -> EType
                   {
                     if (deviceType.indexOf("MediaServer") != -1) {
                       return MediaServer;
                     }
                     if (deviceType.indexOf("MediaRender") != -1) {
                       return MediaRenderer;
                     }
                     if (deviceType.indexOf("DimmableLight") != -1) {
                       return DimmableLight;
                     }
                     if (deviceType.indexOf("Printer") != -1) {
                       return Printer;
                     }
                     if (deviceType.indexOf("Basic") != -1) {
                       return Basic;
                     }
                     if (deviceType.indexOf("InternetGatewayDevice") != -1) {
                       return InternetGateway;
                     }
                     if (deviceType.indexOf("WANDevice") != -1) {
                       return WANDevice;
                     }
                     if (deviceType.indexOf("WANConnectionDevice") != -1) {
                       return WANConnectionDevice;
                     }
                     return Unknown;
                   }()};

  setType(type);
}

void CDevice::setURL(QUrl const& url)
{
  m_d->m_url = url;
}

void CDevice::setUUID(const QString& uuid)
{
  m_d->m_uuid = uuid;
}

void CDevice::setPlaylistStatus(EPlaylistStatus state)
{
  m_d->m_managePlaylists = state;
}

void CDevice::setModelName(const QString& name)
{
  m_d->m_modelName = name;
}

void CDevice::setModelNumber(const QString& number)
{
  m_d->m_modelNumber = number;
}

void CDevice::setModelURL(const QString& url)
{
  m_d->m_modelURL = url;
}

void CDevice::setModelDesc(const QString& desc)
{
  m_d->m_modelDesc = desc;
}

void CDevice::setSerialNumber(const QString& number)
{
  m_d->m_serialNumber = number;
}

void CDevice::setUpc(const QString& upc)
{
  m_d->m_upc = upc;
}

void CDevice::setPresentationURL(const QString& url)
{
  m_d->m_presentationURL = url;
}

void CDevice::setManufacturer(const QString& manufacturer)
{
  m_d->m_manufacturer = manufacturer;
}

void CDevice::setManufacturerURL(const QString& url)
{
  m_d->m_manufacturerURL = url;
}

void CDevice::setFriendlyName(const QString& name)
{
  m_d->m_friendlyName = name;
}

void CDevice::setDeviceType(const QString& type)
{
  m_d->m_deviceType = type;
}

void CDevice::setDlnaDocs(QStringList const& docs)
{
  m_d->m_dlnaDocs = docs;
}

void CDevice::addDlnaDoc(const QString& doc)
{
  m_d->m_dlnaDocs << doc;
}

void CDevice::setDlnaCaps(QStringList const& caps)
{
  m_d->m_dlnaCaps = caps;
}

void CDevice::addPixmap()
{
  m_d->m_pixmaps.push_back(CDevicePixmap());
}

void CDevice::setPixmaps(QList<CDevicePixmap> const& pixmaps)
{
  m_d->m_pixmaps = pixmaps;
}

void CDevice::setMajorVersion(unsigned version)
{
  m_d->m_majorVersion = static_cast<unsigned short>(version);
}

void CDevice::setMinorVersion(unsigned version)
{
  m_d->m_minorVersion = static_cast<unsigned short>(version);
}

void CDevice::setConnectionID(unsigned id)
{
  m_d->m_connectionID = id;
}

void CDevice::setServices(TMServices const& services)
{
  m_d->m_services = services;
}

void CDevice::setParentUUID(const QString& uuid)
{
  m_d->m_parentUUID = uuid;
}

CDevice::EType CDevice::type() const
{
  return static_cast<EType>(m_d->m_type);
}

const QString& CDevice::uuid() const
{
  return m_d->m_uuid;
}

QUrl const& CDevice::url() const
{
  return m_d->m_url;
}

CDevice::EPlaylistStatus CDevice::playlistStatus() const
{
  if (m_d->m_managePlaylists == UnknownHandler) {
    m_d->m_managePlaylists =
        hasProtocol("audio/x-mpegurl") ? PlaylistHandler : NoPlaylistHandler;
  }

  return static_cast<EPlaylistStatus>(m_d->m_managePlaylists);
}

bool CDevice::isSubDevice() const
{
  return !m_d->m_parentUUID.isEmpty();
}

const QString& CDevice::modelName() const
{
  return m_d->m_modelName;
}

const QString& CDevice::modelNumber() const
{
  return m_d->m_modelNumber;
}

const QString& CDevice::modelURL() const
{
  return m_d->m_modelURL;
}

const QString& CDevice::modelDesc() const
{
  return m_d->m_modelDesc;
}

const QString& CDevice::serialNumber() const
{
  return m_d->m_serialNumber;
}

const QString& CDevice::upc() const
{
  return m_d->m_upc;
}

const QString& CDevice::presentationURL() const
{
  return m_d->m_presentationURL;
}

const QString& CDevice::manufacturer() const
{
  return m_d->m_manufacturer;
}

const QString& CDevice::manufacturerURL() const
{
  return m_d->m_manufacturerURL;
}

const QString& CDevice::friendlyName() const
{
  return m_d->m_friendlyName;
}

const QString& CDevice::deviceType() const
{
  return m_d->m_deviceType;
}

QStringList const& CDevice::dlnaDocs() const
{
  return m_d->m_dlnaDocs;
}

QStringList const& CDevice::dlnaCaps() const
{
  return m_d->m_dlnaCaps;
}

QList<CDevicePixmap> const& CDevice::pixmaps() const
{
  return m_d->m_pixmaps;
}

QList<CDevicePixmap>& CDevice::pixmaps()
{
  return m_d->m_pixmaps;
}

unsigned CDevice::minorVersion() const
{
  return m_d->m_minorVersion;
}

unsigned CDevice::majorVersion() const
{
  return m_d->m_majorVersion;
}

unsigned CDevice::highestSupportedVersion() const
{
  unsigned version = 0;
  int index = m_d->m_deviceType.lastIndexOf(':');
  if (index != -1) {
    QString s = m_d->m_deviceType.right(m_d->m_deviceType.length() - index - 1);
    version = s.toUInt();
  }

  return version;
}

unsigned CDevice::connectionID() const
{
  return m_d->m_connectionID;
}

TMServices const& CDevice::services() const
{
  return m_d->m_services;
}

TMServices& CDevice::services()
{
  return m_d->m_services;
}

const QString& CDevice::parentUUID() const
{
  return m_d->m_parentUUID;
}

QString CDevice::name() const
{
  QString name = friendlyName();
  if (name.isEmpty()) {
    name = modelName();
    if (name.isEmpty()) {
      name = url().toString();
    }
  }

  return name;
}

QUrl CDevice::pixmap(EPreferedPixmapType type,
                     char const* mimeType,
                     int width,
                     int height) const
{
  QUrl url;
  QString path;
  int preferedCriteria = width * height * 24;
  QList<CDevicePixmap> const& pixmaps = this->pixmaps();
  if (!pixmaps.isEmpty()) {
    switch (type) {
      case Nearest: {
        int dmin = std::numeric_limits<int>::max();
        for (CDevicePixmap const& pixmap : pixmaps) {
          int dist = pixmap.preferedCriteria() - preferedCriteria;
          if (dist < dmin || (dist == dmin && pixmap.hasMimeType(mimeType))) {
            dmin = dist;
            path = pixmap.url();
          }
        }

        break;
      }

      case SmResol: {
        int cmin = std::numeric_limits<int>::max();
        for (CDevicePixmap const& pixmap : pixmaps) {
          int criteria = pixmap.preferedCriteria();
          if ((criteria < cmin)
              || (criteria == cmin && pixmap.hasMimeType(mimeType))) {
            cmin = criteria;
            path = pixmap.url();
          }
        }

        break;
      }

      case HiResol: {
        int cmax = -std::numeric_limits<int>::max();
        for (CDevicePixmap const& pixmap : pixmaps) {
          int criteria = pixmap.preferedCriteria();
          if ((criteria > cmax)
              || (criteria == cmax && pixmap.hasMimeType(mimeType))) {
            cmax = criteria;
            path = pixmap.url();
          }
        }

        break;
      }
    }
  }

  if (!path.isEmpty()) {
    url = m_d->m_url;
    url.setPath(path);
  }

  return url;
}

QByteArray CDevice::pixmapBytes(QNetworkAccessManager* naMgr,
                                EPreferedPixmapType type,
                                char const* mimeType,
                                int width,
                                int height,
                                int timeout) const
{
  QByteArray bytes;
  QUrl url = pixmap(type, mimeType, width, height);
  if (!url.isEmpty()) {
    bytes = CDataCaller(naMgr).callData(url.toString(), timeout);
  }

  return bytes;
}

bool CDevice::parseXml(QByteArray const& data)
{
  CXmlHDevice h(*this);
  return h.parse(data) | CXmlH::tolerantMode();
}

bool CDevice::extractServiceComponents(QNetworkAccessManager* naMgr,
                                       int timeout)
{
  bool success = false;
  for (TMServices::iterator its = m_d->m_services.begin(),
                            end = m_d->m_services.end();
       its != end;
       ++its)
  {
    CService& service = its->second;
    QString scpdURL = service.scpdURL();
    success = true;
    if (!scpdURL.isEmpty()) {
      QUrl url = m_d->m_url;
      url.setPath(scpdURL);
      CDataCaller dc(naMgr);
      QByteArray data = dc.callData(url.toString(), timeout);
      if (!data.isEmpty()) {
        success = service.parseXml(data);
        if (success) {
          TMStateVariables const& variables = service.stateVariables();
          for (TMStateVariables::const_iterator itv = variables.cbegin(),
                                                end = variables.cend();
               itv != end;
               ++itv)
          {
            CStateVariable const& var = itv.value();
            if (var.isEvented()) {
              service.setEvented(var.isEvented());
              break;
            }
          }
        }
      }
    }
  }

  return success;
}

bool CDevice::hasSubscribed() const
{
  bool subscribed = false;
  for (TMServices::const_iterator it = m_d->m_services.begin(),
                                  end = m_d->m_services.end();
       it != end;
       ++it)
  {
    const auto service {it->second};
    if (!service.subscribeSID().isEmpty()) {
      subscribed = true;
    }
  }

  return subscribed;
}

QList<QString> CDevice::subscribedServices() const
{
  QStringList services;
  for (TMServices::const_iterator it = m_d->m_services.begin(),
                                  end = m_d->m_services.end();
       it != end;
       ++it)
  {
    const auto service {it->second};
    if (!service.subscribeSID().isEmpty()) {
      services << it->first;
    }
  }

  return services;
}

CStateVariable CDevice::stateVariable(const QString& name,
                                      const QString& serviceID) const
{
  CStateVariable variable;
  TMServices const& services = this->services();
  if (serviceID.isEmpty()) {
    for (TMServices::const_iterator it = services.cbegin(),
                                    end = services.end();
         it != end;
         ++it)
    {
      const auto service {it->second};
      auto variables {service.stateVariables()};
      if (variables.contains(name)) {
        variable = variables.value(name);
        break;
      }
    }
  } else {
    if (services.contains(serviceID)) {
      const auto service {services.at(serviceID)};
      variable = service.stateVariable(name);
    }
  }

  return variable;
}

QList<CDevice> const& CDevice::subDevices() const
{
  return m_d->m_subDevices;
}

QList<CDevice>& CDevice::subDevices()
{
  return m_d->m_subDevices;
}

QStringList CDevice::serviceIDs(const QString& actionName) const
{
  QStringList services;
  services.reserve(m_d->m_services.size());
  for (TMServices::const_iterator its = m_d->m_services.cbegin(),
                                  end = m_d->m_services.cend();
       its != end;
       ++its)
  {
    CService const& service = its->second;
    TMActions const& actions = service.actions();
    if (actions.contains(actionName)) {
      services << its->first;
    }
  }

  return services;
}

CAction CDevice::action(const QString& serviceID, const QString& name) const
{
  CAction action;
  if (serviceID.isEmpty()) {
    for (TMServices::const_iterator its = m_d->m_services.cbegin(),
                                    end = m_d->m_services.cend();
         its != end;
         ++its)
    {
      CService const& service = its->second;
      TMActions const& actions = service.actions();
      if (actions.contains(name)) {
        action = actions.value(name);
      }
    }
  } else {
    CService const& service = services().at(serviceID);
    action = service.actions().value(name);
  }

  return action;
}

bool CDevice::hasAction(const QString& serviceID, const QString& name) const
{
  bool success = false;
  if (serviceID.isEmpty()) {
    for (TMServices::const_iterator its = m_d->m_services.cbegin(),
                                    end = m_d->m_services.cend();
         its != end && !success;
         ++its)
    {
      CService const& service = its->second;
      TMActions const& actions = service.actions();
      success = actions.contains(name);
    }
  } else {
    CService const& service = services().at(serviceID);
    success = service.actions().contains(name);
  }

  return success;
}

bool CDevice::hasProtocol(const QString& protocol, bool exact) const
{
  bool found = false;
  CService const& service =
      m_d->m_services.at("urn:upnp-org:serviceId:ConnectionManager");
  CStateVariable variable = service.stateVariable("SinkProtocolInfo");
  QString protocols = variable.value().toString();
  if (protocols.isEmpty()) {
    variable = service.stateVariable("SourceProtocolInfo");
    protocols = variable.value().toString();
  }

  if (!exact) {
    found = protocols.indexOf(protocol) != -1;
  } else {
    QStringList protocolInfos = protocols.split(',');
    found = std::find(protocolInfos.begin(), protocolInfos.end(), protocol)
        != protocolInfos.end();
  }

  return found;
}

CDevice CDevice::subDevice(const QString& uuid) const
{
  CDevice subDevice;
  for (CDevice const& device : m_d->m_subDevices) {
    if (device.uuid() == uuid) {
      subDevice = device;
      break;
    }
  }

  return subDevice;
}

bool CDevice::hasSubDevice(const QString& uuid) const
{
  bool success = false;
  for (CDevice const& device : m_d->m_subDevices) {
    if (device.uuid() == uuid) {
      success = true;
      break;
    }
  }

  return success;
}

void CDevice::setSubscribtionDisabled(QStringList const& ids, bool disable)
{
  bool enable = !disable;
  for (const QString& id : ids) {
    if (m_d->m_services.contains(id)) {
      m_d->m_services[id].setEvented(enable);
    }
  }
}

QStringList CDevice::eventedServices() const
{
  QStringList ids;
  for (TMServices::const_iterator its = m_d->m_services.cbegin(),
                                  end = m_d->m_services.cend();
       its != end;
       ++its)
  {
    CService const& service = its->second;
    if (service.isEvented()) {
      ids << its->first;
    }
  }

  return ids;
}

void CDevice::replaceSubDevices(QList<CDevice> const& devices)
{
  m_d->m_subDevices = devices;
}
