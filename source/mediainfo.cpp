#include "mediainfo.hpp"

#include "xmlhdidllite.hpp"

namespace QtUPnP
{

/*! \brief Internal structure of CMediaInfo. */
struct SMediaInfoData : public QSharedData
{
  SMediaInfoData() = default;

  unsigned m_nrTracks {0};
  QString m_mediaDuration;
  QString m_currentURI;
  QString m_currentURIMetaData;
  QString m_nextURI;
  QString m_nextURIMetaData;
  QString m_playMedium;
  QString m_recordMedium;
  QString m_writeStatus;
};

}  // namespace QtUPnP

using namespace QtUPnP;

CMediaInfo::CMediaInfo()
    : m_d {new SMediaInfoData}
{
}

CMediaInfo::CMediaInfo(CMediaInfo const& rhs)
    : m_d {rhs.m_d}
{
}

CMediaInfo& CMediaInfo::operator=(CMediaInfo const& rhs)
{
  if (this != &rhs) {
    m_d.operator=(rhs.m_d);
  }

  return *this;
}

CMediaInfo::~CMediaInfo() = default;

void CMediaInfo::setNrTracks(unsigned nrTracks)
{
  m_d->m_nrTracks = nrTracks;
}

void CMediaInfo::setMediaDuration(const QString& duration)
{
  m_d->m_mediaDuration = duration;
}

void CMediaInfo::setCurrentURI(const QString& uri)
{
  m_d->m_currentURI = uri;
}

void CMediaInfo::setCurrentURIMetaData(const QString& metadata)
{
  m_d->m_currentURIMetaData = metadata;
}

void CMediaInfo::setNextURI(const QString& uri)
{
  m_d->m_nextURI = uri;
}

void CMediaInfo::setNextURIMetaData(const QString& metadata)
{
  m_d->m_nextURIMetaData = metadata;
}

void CMediaInfo::setPlayMedium(const QString& medium)
{
  m_d->m_playMedium = medium;
}

void CMediaInfo::setRecordMedium(const QString& medium)
{
  m_d->m_recordMedium = medium;
}

void CMediaInfo::setWriteStatus(const QString& status)
{
  m_d->m_writeStatus = status;
}

unsigned CMediaInfo::nrTracks() const
{
  return m_d->m_nrTracks;
}

const QString& CMediaInfo::mediaDuration() const
{
  return m_d->m_mediaDuration;
}

const QString& CMediaInfo::currentURI() const
{
  return m_d->m_currentURI;
}

const QString& CMediaInfo::currentURIMetaData() const
{
  return m_d->m_currentURIMetaData;
}

const QString& CMediaInfo::nextURI() const
{
  return m_d->m_nextURI;
}

const QString& CMediaInfo::nextURIMetaData() const
{
  return m_d->m_nextURIMetaData;
}

const QString& CMediaInfo::playMedium() const
{
  return m_d->m_playMedium;
}

const QString& CMediaInfo::recordMedium() const
{
  return m_d->m_recordMedium;
}

const QString& CMediaInfo::writeStatus() const
{
  return m_d->m_writeStatus;
}

CDidlItem CMediaInfo::didlItem(const QString& metaData)
{
  CXmlHDidlLite h;
  return h.firstItem(metaData);
}

CDidlItem CMediaInfo::currentURIDidlItem() const
{
  return didlItem(m_d->m_currentURIMetaData);
}

CDidlItem CMediaInfo::nextURIDidlItem() const
{
  return didlItem(m_d->m_nextURIMetaData);
}
