#include "actioninfo.hpp"

namespace
{
const auto DStartEnvelopeBody {QStringLiteral(
    "<s:Envelope s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" \
                 xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body>")};
const auto DActionserviceID {QStringLiteral("<u:%1 xmlns:u=\"%2\">")};
const auto DAction0 {QStringLiteral("</u:%1>")};
const auto DEndBodyEnvelope {QStringLiteral("</s:Body></s:Envelope>")};
}  // namespace

namespace QtUPnP
{

/*! \brief Internal structure of CActionInfo. */
struct SActionInfoData : public QSharedData
{
  SActionInfoData() = default;

  bool m_success {false};
  QString m_deviceUUID;
  QString serviceID;
  QString m_actionName;
  QString m_message;
  QByteArray m_response;
};

CActionInfo::CActionInfo()
    : m_d {new SActionInfoData}
{
}

CActionInfo::CActionInfo(CActionInfo const& other)
    : m_d {other.m_d}
{
}

CActionInfo& CActionInfo::operator=(CActionInfo const& other)
{
  if (this != &other) {
    m_d.operator=(other.m_d);
  }

  return *this;
}

CActionInfo::~CActionInfo() = default;

void CActionInfo::startMessage(const QString& deviceUUID,
                               const QString& serviceID,
                               const QString& action)
{
  m_d->m_deviceUUID = deviceUUID;
  m_d->serviceID = serviceID;
  m_d->m_actionName = action;
  m_d->m_message = DStartEnvelopeBody + DActionserviceID.arg(action, serviceID);
}

void CActionInfo::endMessage()
{
  m_d->m_message += DAction0.arg(m_d->m_actionName) + DEndBodyEnvelope;
}

void CActionInfo::addArgument(const QString& name, const QString& value)
{
  m_d->m_message += QString("<%1>%2</%1>").arg(name, value);
}

bool CActionInfo::succeeded() const
{
  return m_d->m_success;
}

QString CActionInfo::deviceUUID() const
{
  return m_d->m_deviceUUID;
}

QString CActionInfo::serviceID() const
{
  return m_d->serviceID;
}

QString CActionInfo::actionName() const
{
  return m_d->m_actionName;
}

QString CActionInfo::message() const
{
  return m_d->m_message;
}

QByteArray CActionInfo::response() const
{
  return m_d->m_response;
}

void CActionInfo::setSucceeded(bool success)
{
  m_d->m_success = success;
}

void CActionInfo::setDeviceUUID(const QString& uuid)
{
  m_d->m_deviceUUID = uuid;
}

void CActionInfo::setserviceID(const QString& type)
{
  m_d->serviceID = type;
}

void CActionInfo::setActionName(const QString& name)
{
  m_d->m_actionName = name;
}

void CActionInfo::setMessage(const QString& message)
{
  m_d->m_message = message;
}

void CActionInfo::setResponse(const QByteArray& response)
{
  m_d->m_response = response;
}

}  // namespace QtUPnP
