#include "xmlhdidllite.hpp"

using namespace QtUPnP;

CXmlHDidlLite::CXmlHDidlLite() = default;

bool CXmlHDidlLite::startElement(const QString& namespaceURI,
                                 const QString& localName,
                                 const QString& qName,
                                 const QXmlAttributes& atts)
{
  if (qName != "DIDL-Lite") {
    CXmlH::startElement(namespaceURI, localName, qName, atts);

    TMProps props;
    for (int iAtt = 0; iAtt < atts.count(); ++iAtt) {
      const QString& name = atts.qName(iAtt);
      const QString& value = atts.value(iAtt);
      props[name] = value;
    }

    CDidlElem elem;
    elem.setProps(props);
    if (qName == "item" || qName == "container") {
      CDidlItem item;
      item.insert(qName, elem);
      m_items.push_back(item);
    } else if (!m_items.isEmpty()) {
      m_items.last().insert(qName, elem);
    }
  }

  return true;
}

bool CXmlHDidlLite::characters(const QString& name)
{
  if (!m_stack.isEmpty() && !m_items.isEmpty() && !name.trimmed().isEmpty()) {
    const QString& tag = m_stack.top();
    CDidlItem& item = m_items.last();
    CDidlElem elem = item.value(tag);
    elem.setValue(name);
    item.replace(tag, elem);
  }

  return true;
}

CDidlItem CXmlHDidlLite::firstItem(QByteArray const& data)
{
  CDidlItem item;
  bool success = parse(data);
  if (success && !m_items.isEmpty()) {
    item = m_items.first();
  }

  return item;
}

CDidlItem CXmlHDidlLite::firstItem(const QString& didlLite)
{
  CDidlItem item;
  if (!didlLite.isEmpty() && didlLite != "NOT_IMPLEMENTED") {
    QString data = ampersandHandler(didlLite);
    bool success = parse(data);
    if (success && !m_items.isEmpty()) {
      item = m_items.first();
    }
  }

  return item;
}
