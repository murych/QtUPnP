#ifndef CONTROL_HPP
#define CONTROL_HPP

#include <QStringList>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

class CControlPoint;
class CActionInfo;

/*! \brief The base class for UPnP/AV specific objects (CConnectionManager,
 * CAVTransport...).
 *
 * The CControl class is the base class for
 * \li CConnectionManager
 * \li CContentDirectory
 * \li
 * \li CRenderingContro
 */
class QTUPNP_EXPORT CControl
{
public:
  /*! Default constructor. */
  CControl() = default;

  /*! Constructor. */
  explicit CControl(CControlPoint* cp)
      : m_cp {cp}
  {
  }

  /*! Sets the application control point. */
  void setControlPoint(CControlPoint* cp) { m_cp = cp; }

  /*! Returns the control point. */
  [[nodiscard]] auto* controlPoint() { return m_cp; }

  /*! Returns a string list corresponding at CVS format (values separated by
   * comma). */
  [[nodiscard]] QStringList fromCVS(QString entryString);

  /*! Returns a boolean value corresponding at a string.
   * True is returned if entryString doesn't start by '0' or 'f' or 'F';
   */
  static bool toBool(const QString& entryString);

protected:
  CControlPoint* m_cp {nullptr};  //<! The control point of the application.
};

}  // namespace QtUPnP

#endif  // CONTROL_HPP
