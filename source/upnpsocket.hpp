#ifndef UPNP_SOCKET_HPP
#define UPNP_SOCKET_HPP 1

#include <QUdpSocket>
#include <QUrl>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

/*! \brief The base class of CUnicastSocket and CMulticastSocket. */
class QTUPNP_EXPORT CUpnpSocket : public QUdpSocket
{
public:
  /*! \brief The structure holds information about the device that extracted
   * from datagram using UDP sockets.*/
  struct SNDevice
  {
    /*! The device message type. */
    enum EType
    {
      Unknown,  //!< Unknown
      Notify,  //!< NOTIFY * HTTP/1.1
      Search,  //!< M-SEARCH * HTTP/1.1
      Response,  //!< HTTP/1.1
      Byebye  //!< The NTS: header is ssdp:byebye
    };

    /*! Default constructor. */
    SNDevice(EType type = Unknown);

    /*! Constructor.
     *
     * \param type: The type.
     * \param url: The url of the device.
     * \param uuid: The uuid of the device.
     */
    SNDevice(EType type, const QUrl& url, const QString& uuid);

    /*! Copy constructor. */
    SNDevice(SNDevice const& other);

    /*! Equal operator. */
    auto operator=(SNDevice const& other) -> SNDevice&;

    /*! Equality operator. */
    auto operator==(SNDevice const& other) const -> bool;

    EType m_type;  //!< The type.
    QUrl m_url;  //!< The url.
    QString m_uuid;  //!< The uuid.
  };

  /*! Default constructor. */
  CUpnpSocket(QObject* parent = nullptr);

  /*! Destructor. */
  ~CUpnpSocket() override;

  /*! Returns the device host address. */
  [[nodiscard]] auto senderAddress() const { return m_senderAddr; }

  /*! Returns the port used by the device. */
  [[nodiscard]] auto senderPort() const { return m_senderPort; }

  /*! Returns the current datagram. */
  [[nodiscard]] auto datagram() const { return m_datagram; }

  /*! Returns the datagram. */
  [[nodiscard]] auto readDatagrams() -> QByteArray;

  /*! Decodes the datagram. */
  void decodeDatagram();

  /*! Launchs the M-SEARCH udp datagram. */
  auto discover(const QHostAddress& hostAddress,
                uint16_t port,
                uint16_t mx = 5,
                char const* uuid = nullptr) -> bool;

  /*! Clear the device list. */
  void resetDevices() { m_devices.clear(); }

  /*! Returns the device list. */
  [[nodiscard]] auto devices() const { return m_devices; }

  /*! Sets the user name.
   * \param name: The user name.
   */
  void setName(const QString& name) { m_name = name; }

  /*! Returns the user name. */
  [[nodiscard]] auto name() { return m_name; }

  /*! Returns the local host address from router.
   * This function is used in case of multiple network interfaces.
   */
  static QHostAddress localHostAddressFromRouter();

  /*! Sets IPV4 addresses to ignore.
   * For several router, some addresses are for internal used. e.g.
   * 192.168.0.10. But the router indicates it is a root device and fails when
   * QtUPnP asks the device caracteristics. Moreover, the device is long to
   * respond. The discovery becomes very slow. You can add somes addresses to
   * ignore during the discovery or for "NOTIFY" upd messages.
   */
  static void setSkippedAddresses(const QList<QByteArray>& addresses);

  /*! Returns IPV4 addresses to ignore (see above). */
  static auto skippedAddresses() { return m_skippedAddresses; }

  /*! Adds IPV4 addresses to ignore (see above). */
  static void addSkippedAddresses(const QByteArray& addr)
  {
    m_skippedAddresses.append(addr);
  }

  /*! Clear the list of IP Addresses to ignored. */
  static void clearSkippedAddresses() { m_skippedAddresses.clear(); }

  /*! Returns the list of uuid to ignored. */
  static auto skippedUUIDs() { return m_skippedUUIDs; }

  /*! Adds a device uuid to ignored.
   * \param uuid: uuid can be partial. E.g for a gateway device with an uuid=,
   * "uuid:upnp-InternetGatewayDevice-1_0-8cb9363e1228",
   * "uuid=gatewaydevice" is valid.
   */
  static void addSkippedUUID(const QByteArray& uuid);

  /*! Clear the list of device uuids to ignored. */
  static void clearSkippedUUID() { m_skippedUUIDs.clear(); }

  /*! Returns the local host address.
   * This function returns the local host address of the first interface and
   * different of 127.0.0.1.
   */
  static auto localHostAddress(bool ipv6 = false) -> QHostAddress;

  /*! Returns the http header value of User-Agent. */
  static auto userAgent() -> QByteArray;

private:
  /*! See CUpnpSocket::readDatagrams comment. */
  enum EWait
  {
    StartingWait = 30
  };

  /*! Adds a device in the device list. */
  void addDevice(const SNDevice& device);

  /*! Creates a device from a datagram. */
  auto createDevice(const QByteArray& datagram) -> SNDevice;

private:
  static QHostAddress m_localHostAddress;  //!< The local host address.
  static QHostAddress m_localHostAddress6;  //!< The local host address.
  static QList<QByteArray> m_skippedUUIDs;  //!< uuid to ignored.
  static QList<QByteArray> m_skippedAddresses;  //!< IPV4 addresses to ignore.

private:
  QHostAddress m_senderAddr;  //!< Host address of the device.
  uint16_t m_senderPort {0};  //!< The port used by the device.
  QByteArray m_datagram;  //!< The current datagram.
  QList<SNDevice> m_devices;  //!< The list of devices.
  QString m_name;  //!< Socket user name.
};

}  // namespace QtUPnP

#endif
