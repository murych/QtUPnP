#include "unicastsocket.hpp"

#include "helper.hpp"

using namespace QtUPnP;

CUnicastSocket::CUnicastSocket(QObject* parent)
    : CUpnpSocket {parent}
{
}

uint16_t CUnicastSocket::bind(const QHostAddress& addr)
{
  uint16_t port {0};
  // The range is specified by the UDA 1.1 standard
  for (port = 49152; port < 65535; ++port) {
    if (QUdpSocket::bind(addr, port, QAbstractSocket::DontShareAddress)) {
      break;
    }
  }

  if (port == 65535 || state() != QUdpSocket::BoundState) {
    qDebug() << "CUnicastSocket::bind (no ports free):"
             << addr.toString().toLatin1() + ':' + QByteArray::number(port);
    port = 0;
  }

  return port;
}
