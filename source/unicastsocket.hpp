#ifndef UNICAST_SOCKET_HPP
#define UNICAST_SOCKET_HPP 1

#include "upnpsocket.hpp"

namespace QtUPnP
{

/*! \brief Provides the mechanism to manage unicast sockets. */
class CUnicastSocket : public CUpnpSocket
{
public:
  /*! Default constructor. */
  explicit CUnicastSocket(QObject* parent = nullptr);

  /*! Destructor. */
  ~CUnicastSocket() override = default;

  /*! Binds to IPV4 address on addr.
   * \param addr: The host address.
   * \return The port or 0 in case of failure.
   */
  auto bind(const QHostAddress& addr) -> uint16_t;
};

}  // namespace QtUPnP

#endif
