#include "renderingcontrol.hpp"

#include "actioninfo.hpp"
#include "controlpoint.hpp"

using namespace QtUPnP;

QStringList CRenderingControl::getListPresets(const QString& rendererUUID,
                                              unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QStringList presets;
  QList<CControlPoint::TArgValue> args;
  args.reserve(2);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue("CurrentPresetNameList", QString());
  CActionInfo actionInfo =
      m_cp->invokeAction(rendererUUID, "ListPresets", args);
  if (actionInfo.succeeded()) {
    presets = fromCVS(args.last().second);
  }

  return presets;
}

bool CRenderingControl::getMute(const QString& rendererUUID,
                                const QString& channel,
                                unsigned instanceID)
{
  return getBoolChannel(
      rendererUUID, "GetMute", "CurrentMute", channel, instanceID);
}

int CRenderingControl::getVolume(const QString& rendererUUID,
                                 const QString& channel,
                                 unsigned instanceID)
{
  return getVolume(rendererUUID, "GetVolume", channel, instanceID);
}

bool CRenderingControl::selectPreset(const QString& rendererUUID,
                                     const QString& preset,
                                     unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  m_cp->abortStreaming();
  QList<CControlPoint::TArgValue> args;
  args.reserve(2);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue("PresetName", preset);
  CActionInfo actionInfo =
      m_cp->invokeAction(rendererUUID, "SelectPreset", args);
  return actionInfo.succeeded();
}

bool CRenderingControl::setMute(const QString& rendererUUID,
                                bool value,
                                const QString& channel,
                                unsigned instanceID)
{
  return setBoolChannel(
      rendererUUID, "SetMute", value, "DesiredMute", channel, instanceID);
}

bool CRenderingControl::setVolume(const QString& rendererUUID,
                                  unsigned value,
                                  const QString& channel,
                                  unsigned instanceID)
{
  return setVolume(rendererUUID, "SetVolume", value, channel, instanceID);
}

unsigned CRenderingControl::getBlueVideoBlackLevel(const QString& rendererUUID,
                                                   unsigned instanceID)
{
  return getUInt1(rendererUUID,
                  "GetBlueVideoBlackLevel",
                  "CurrentBlueVideoBlackLevel",
                  instanceID);
}

unsigned CRenderingControl::getBlueVideoGain(const QString& rendererUUID,
                                             unsigned instanceID)
{
  return getUInt1(
      rendererUUID, "GetBlueVideoGain", "CurrentBlueVideoGain", instanceID);
}

unsigned CRenderingControl::getBrightness(const QString& rendererUUID,
                                          unsigned instanceID)
{
  return getUInt1(
      rendererUUID, "GetBrightness", "CurrentBrightness", instanceID);
}

unsigned CRenderingControl::getColorTemperature(const QString& rendererUUID,
                                                unsigned instanceID)
{
  return getUInt1(rendererUUID,
                  "GetColorTemperature",
                  "CurrentColorTemperature",
                  instanceID);
}

unsigned CRenderingControl::getContrast(const QString& rendererUUID,
                                        unsigned instanceID)
{
  return getUInt1(rendererUUID, "GetContrast", "CurrentContrast", instanceID);
}

unsigned CRenderingControl::getGreenVideoBlackLevel(const QString& rendererUUID,
                                                    unsigned instanceID)
{
  return getUInt1(rendererUUID,
                  "GetGreenVideoBlackLevel",
                  "CurrentGreenVideoBlackLevel",
                  instanceID);
}

unsigned CRenderingControl::getGreenVideoGain(const QString& rendererUUID,
                                              unsigned instanceID)
{
  return getUInt1(
      rendererUUID, "GetGreenVideoGain", "CurrentGreenVideoGain", instanceID);
}

int CRenderingControl::getHorizontalKeystone(const QString& rendererUUID,
                                             unsigned instanceID)
{
  return getUInt1(rendererUUID,
                  "GetHorizontalKeystone",
                  "CurrentHorizontalKeystone",
                  instanceID);
}

bool CRenderingControl::getLoudness(const QString& rendererUUID,
                                    const QString& channel,
                                    unsigned instanceID)
{
  return getBoolChannel(
      rendererUUID, "GetLoudness", "CurrentLoudness", channel, instanceID);
}

unsigned CRenderingControl::getRedVideoBlackLevel(const QString& rendererUUID,
                                                  unsigned instanceID)
{
  return getUInt1(rendererUUID,
                  "GetRedVideoBlackLevel",
                  "CurrentRedVideoBlackLevel",
                  instanceID);
}

unsigned CRenderingControl::getRedVideoGain(const QString& rendererUUID,
                                            unsigned instanceID)
{
  return getUInt1(
      rendererUUID, "GetRedVideoGain", "CurrentRedVideoGain", instanceID);
}

unsigned CRenderingControl::getSharpness(const QString& rendererUUID,
                                         unsigned instanceID)
{
  return getUInt1(rendererUUID, "GetSharpness", "CurrentSharpness", instanceID);
}

int CRenderingControl::getVerticalKeystone(const QString& rendererUUID,
                                           unsigned instanceID)
{
  return getUInt1(rendererUUID,
                  "GetVerticalKeystone",
                  "CurrentVerticalKeystone",
                  instanceID);
}

int CRenderingControl::getVolumeDB(const QString& rendererUUID,
                                   const QString& channel,
                                   unsigned instanceID)
{
  return getVolume(rendererUUID, "GetVolumeDB", channel, instanceID);
}

QPair<int, int> CRenderingControl::getVolumeDBRange(const QString& rendererUUID,
                                                    const QString& channel,
                                                    unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(2);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue("Channel", channel);
  args << CControlPoint::TArgValue("MinValue", QString());
  args << CControlPoint::TArgValue("MaxValue", QString());
  CActionInfo actionInfo =
      m_cp->invokeAction(rendererUUID, "GetVolumeDBRange", args);
  QPair<int, int> values;
  if (actionInfo.succeeded()) {
    values.first = (*(args.end() - 2)).second.toInt();
    values.second = args.last().second.toInt();
  }

  return values;
}

bool CRenderingControl::setBlueVideoBlackLevel(const QString& rendererUUID,
                                               unsigned value,
                                               unsigned instanceID)
{
  return setUInt1(rendererUUID,
                  "SetBlueVideoBlackLevel",
                  "DesiredBlueVideoBlackLevel",
                  value,
                  instanceID);
}

bool CRenderingControl::setBlueVideoGain(const QString& rendererUUID,
                                         unsigned value,
                                         unsigned instanceID)
{
  return setUInt1(rendererUUID,
                  "SetBlueVideoGain",
                  "DesiredBlueVideoGain",
                  value,
                  instanceID);
}

bool CRenderingControl::setBrightness(const QString& rendererUUID,
                                      unsigned value,
                                      unsigned instanceID)
{
  return setUInt1(
      rendererUUID, "SetBrightness", "DesiredBrightness", value, instanceID);
}

bool CRenderingControl::setColorTemperature(const QString& rendererUUID,
                                            unsigned value,
                                            unsigned instanceID)
{
  return setUInt1(rendererUUID,
                  "SetColorTemperature",
                  "DesiredColorTemperature",
                  value,
                  instanceID);
}

bool CRenderingControl::setContrast(const QString& rendererUUID,
                                    unsigned value,
                                    unsigned instanceID)
{
  return setUInt1(
      rendererUUID, "SetContrast", "DesiredContrast", value, instanceID);
}

bool CRenderingControl::setGreenVideoBlackLevel(const QString& rendererUUID,
                                                unsigned value,
                                                unsigned instanceID)
{
  return setUInt1(rendererUUID,
                  "SetGreenVideoBlackLevel",
                  "DesiredGreenVideoBlackLevel",
                  value,
                  instanceID);
}

bool CRenderingControl::setGreenVideoGain(const QString& rendererUUID,
                                          unsigned value,
                                          unsigned instanceID)
{
  return setUInt1(rendererUUID,
                  "SetGreenVideoGain",
                  "DesiredGreenVideoGain",
                  value,
                  instanceID);
}

bool CRenderingControl::setHorizontalKeystone(const QString& rendererUUID,
                                              int value,
                                              unsigned instanceID)
{
  return setInt1(rendererUUID,
                 "SetHorizontalKeystone",
                 "DesiredHorizontalKeystone",
                 value,
                 instanceID);
}

bool CRenderingControl::setLoudness(const QString& rendererUUID,
                                    bool value,
                                    const QString& channel,
                                    unsigned instanceID)
{
  return setBoolChannel(rendererUUID,
                        "SetLoudness",
                        value,
                        "DesiredLoudness",
                        channel,
                        instanceID);
}

bool CRenderingControl::setRedVideoBlackLevel(const QString& rendererUUID,
                                              unsigned value,
                                              unsigned instanceID)
{
  return setUInt1(rendererUUID,
                  "SetRedVideoBlackLevel",
                  "DesiredRedVideoBlackLevel",
                  value,
                  instanceID);
}

bool CRenderingControl::setRedVideoGain(const QString& rendererUUID,
                                        unsigned value,
                                        unsigned instanceID)
{
  return setUInt1(rendererUUID,
                  "SetRedVideoGain",
                  "DesiredRedVideoGain",
                  value,
                  instanceID);
}

bool CRenderingControl::setSharpness(const QString& rendererUUID,
                                     unsigned value,
                                     unsigned instanceID)
{
  return setInt1(
      rendererUUID, "SetSharpness", "DesiredSharpness", value, instanceID);
}

bool CRenderingControl::setVerticalKeystone(const QString& rendererUUID,
                                            int value,
                                            unsigned instanceID)
{
  return setInt1(rendererUUID,
                 "SetVerticalKeystone",
                 "DesiredVerticalKeystone",
                 value,
                 instanceID);
}

bool CRenderingControl::setVolumeDB(const QString& rendererUUID,
                                    int value,
                                    const QString& channel,
                                    unsigned instanceID)
{
  return setVolume(rendererUUID, "SetVolumeDB", value, channel, instanceID);
}

unsigned CRenderingControl::getUInt1(const QString& rendererUUID,
                                     const QString& action,
                                     const QString& arg,
                                     unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue(arg, QString());
  unsigned value = 0;
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  if (actionInfo.succeeded()) {
    value = args.last().second.toUInt();
  }

  return value;
}

int CRenderingControl::getInt1(const QString& rendererUUID,
                               const QString& action,
                               const QString& arg,
                               unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue(arg, QString());
  int value = 0;
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  if (actionInfo.succeeded()) {
    value = args.last().second.toInt();
  }

  return value;
}

bool CRenderingControl::setUInt1(const QString& rendererUUID,
                                 const QString& action,
                                 const QString& arg,
                                 unsigned value,
                                 unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue(arg, QString::number(value));
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  return actionInfo.succeeded();
}

bool CRenderingControl::setInt1(const QString& rendererUUID,
                                const QString& action,
                                const QString& arg,
                                int value,
                                unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue(arg, QString::number(value));
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  return actionInfo.succeeded();
}

bool CRenderingControl::setVolume(const QString& rendererUUID,
                                  const QString& action,
                                  int value,
                                  const QString& channel,
                                  unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue("Channel", channel);
  QString volume = QString("%1").arg(value);
  args << CControlPoint::TArgValue("DesiredVolume", volume);
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  return actionInfo.succeeded();
}

int CRenderingControl::getVolume(const QString& rendererUUID,
                                 const QString& action,
                                 const QString& channel,
                                 unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue("Channel", channel);
  args << CControlPoint::TArgValue("CurrentVolume", QString());
  int volume = 0;
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  if (actionInfo.succeeded()) {
    volume = args.last().second.toInt();
  }

  return volume;
}

bool CRenderingControl::setBoolChannel(const QString& rendererUUID,
                                       const QString& action,
                                       bool value,
                                       const QString& arg,
                                       const QString& channel,
                                       unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue("Channel", channel);
  args << CControlPoint::TArgValue(arg, value ? "1" : "0");
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  return actionInfo.succeeded();
}

bool CRenderingControl::getBoolChannel(const QString& rendererUUID,
                                       const QString& action,
                                       const QString& arg,
                                       const QString& channel,
                                       unsigned instanceID)
{
  Q_ASSERT(m_cp != nullptr);
  QList<CControlPoint::TArgValue> args;
  args.reserve(3);
  args << CControlPoint::TArgValue("InstanceID", QString::number(instanceID));
  args << CControlPoint::TArgValue("Channel", channel);
  args << CControlPoint::TArgValue(arg, QString());
  bool value = false;
  CActionInfo actionInfo = m_cp->invokeAction(rendererUUID, action, args);
  if (actionInfo.succeeded()) {
    value = toBool(args.last().second);
  }

  return value;
}
