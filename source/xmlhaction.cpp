#include "xmlhaction.hpp"

#include "statevariable.hpp"

using namespace QtUPnP;

CXmlHAction::CXmlHAction(const QString& actionName,
                         QMap<QString, QString>& vars)
    : m_actionName {actionName}
    , m_vars {vars}
{
}

bool CXmlHAction::characters(const QString& name)
{
  const auto tag {m_stack.top()};
  if (tag == "u:errorCode") {
    m_errorCode = name.toInt();
  } else if (tag == "u:errorDescription") {
    m_errorDesc = name;
  } else {
    const auto pTag {tagParent()};
    if (pTag.endsWith(m_actionName + "Response")) {
      m_vars.insert(tag, name);
    }
  }

  return true;
}
