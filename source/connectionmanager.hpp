#ifndef CONNECTION_MANAGER_HPP
#define CONNECTION_MANAGER_HPP

#include <QVector>

#include "connectioninfo.hpp"
#include "control.hpp"
#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

class CControlPoint;
class CActionInfo;

/*! \brief A wrapper that manages the
 * urn:schemas-upnp-org:service:ConnectionManager:1 service actions.
 */
class QTUPNP_EXPORT CConnectionManager : public CControl
{
public:
  /*! Defines the 2 directions for the protocols. */
  enum EProtocolDir
  {
    Source,
    Sink
  };

  /*! Default constructor. */
  CConnectionManager() = default;

  /*! Constructor
   * \param cp: The control point of the application.
   */
  explicit CConnectionManager(CControlPoint* cp)
      : CControl {cp}
  {
  }

  /*! Invokes the GetCurrentConnectionInfo action.
   * \param deviceUUID: Device uuid.
   * \return The connection info.
   */
  [[nodiscard]] CConnectionInfo getCurrentConnectionInfo(
      const QString& deviceUUID);

  /*! Invokes the >GetProtocolInfos action.
   * \param deviceUUID: Renderer uuid.
   * \return protocol info.
   */
  [[nodiscard]] QVector<QStringList> getProtocolInfos(
      const QString& deviceUUID);

  /*! Invokes the GetCurrentConnectionIDs action.
   * \param deviceUUID: Renderer uuid.
   * \return Current connection IDs.
   */
  [[nodiscard]] QStringList getCurrentConnectionIDs(const QString& deviceUUID);
};

}  // namespace QtUPnP

#endif  // CONNECTION_MANAGER_HPP
