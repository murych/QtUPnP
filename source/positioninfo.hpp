#ifndef POSITION_INFO_HPP
#define POSITION_INFO_HPP

#include <QSharedDataPointer>

#include "didlitem.hpp"

namespace QtUPnP
{

struct SPositionInfoData;

/*! \brief The CPositionInfo class holds information about GetPositionInfo
 * action.
 *
 * See http://upnp.org/specs/av/UPnP-av-AVTransport-Service.pdf.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CPositionInfo
{
public:
  /*! Default constructor. */
  CPositionInfo();

  /*! Copy constructor. */
  CPositionInfo(const CPositionInfo&);

  /*! Equal operator. */
  CPositionInfo& operator=(const CPositionInfo&);

  /*! Destructor. */
  ~CPositionInfo();

  /*! Sets the track number. */
  void setTrack(unsigned track);

  /*! Sets the track duration. */
  void setTrackDuration(const QString& trackDuration);

  /*! Sets the track metadata. */
  void setTrackMetaData(const QString& trackMetaData);

  /*! Sets the track uri. */
  void setTrackURI(const QString& trackURI);

  /*! Sets the track real time. */
  void setRelTime(const QString& relTime);

  /*! Sets the track absolute time. */
  void setAbsTime(const QString& absTime);

  /*! Sets the track real count. */
  void setRelCount(int relCount);

  /*! Sets the track absolute count. */
  void setAbsCount(int absCount);

  /*! Returns the track number. */
  unsigned track() const;

  /*! Returns the track duration. */
  const QString& trackDuration() const;

  /*! Returns the track metadata. */
  const QString& trackMetaData() const;

  /*! Returns the track uri. */
  const QString& trackURI() const;

  /*! Returns the track real time. */
  const QString& relTime() const;

  /*! Returns the track metadata. */
  const QString& absTime() const;

  /*! Returns the track real count. */
  int relCount() const;

  /*! Returns the track absolute count. */
  int absCount() const;

  /*! Converts the track metadata in a CDidlItem. */
  CDidlItem didlItem() const;

private:
  QSharedDataPointer<SPositionInfoData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // POSITION_INFO_HPP
