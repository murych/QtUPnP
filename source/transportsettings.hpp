#ifndef TRANSPORT_SETTINGS_HPP
#define TRANSPORT_SETTINGS_HPP

#include <QSharedDataPointer>

#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

struct STransportSettingsData;

/*! \brief The CTransportSettings class holds information about
 * GetTransportSettings action.
 *
 * See http://upnp.org/specs/av/UPnP-av-AVTransport-Service.pdf.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CTransportSettings
{
public:
  /*! Default constructor. */
  CTransportSettings();

  /*! Copy constructor. */
  CTransportSettings(const CTransportSettings&);

  /*! Equal operator. */
  auto operator=(const CTransportSettings&) -> CTransportSettings&;

  /*! Destructor. */
  ~CTransportSettings();

  /*! Sets the play mode. */
  void setPlayMode(const QString& mode);

  /*! Sets the record quality. */
  void setRecQualityMode(const QString& mode);

  /*! Returns the play mode. */
  [[nodiscard]] auto playMode() const -> QString;

  /*! Returns the record quality. */
  [[nodiscard]] auto recQualityMode() const -> QString;

private:
  QSharedDataPointer<STransportSettingsData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // TRANSPORT_SETTINGS_HPP
