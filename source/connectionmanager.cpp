#include "connectionmanager.hpp"

#include "actioninfo.hpp"
#include "controlpoint.hpp"

namespace QtUPnP
{

QStringList CConnectionManager::getCurrentConnectionIDs(
    const QString& deviceUUID)
{
  Q_ASSERT(m_cp != nullptr);
  QStringList ids;
  QList<CControlPoint::TArgValue> args;
  args << CControlPoint::TArgValue("ConnectionIDs", QString());
  CActionInfo actionInfo =
      m_cp->invokeAction(deviceUUID, "GetCurrentConnectionIDs", args);
  if (actionInfo.succeeded()) {
    ids = fromCVS(args.last().second);
    if (!ids.isEmpty() && ids.first() != "0") {
      CDevice& device = m_cp->device(deviceUUID);
      device.setConnectionID(ids.first().toUInt());
    }
  }

  return ids;
}

QVector<QStringList> CConnectionManager::getProtocolInfos(
    const QString& deviceUUID)
{
  Q_ASSERT(m_cp != nullptr);
  QVector<QStringList> protocols(2);
  QList<CControlPoint::TArgValue> args;
  args.reserve(2);
  args << CControlPoint::TArgValue("Source", {});
  args << CControlPoint::TArgValue("Sink", {});

  auto actionInfo {m_cp->invokeAction(deviceUUID, "GetProtocolInfo", args)};
  if (actionInfo.succeeded()) {
    protocols[Source] = fromCVS(args[0].second);
    protocols[Sink] = fromCVS(args[1].second);
  }

  return protocols;
}

CConnectionInfo CConnectionManager::getCurrentConnectionInfo(
    const QString& deviceUUID)
{
  Q_ASSERT(m_cp != nullptr);
  CConnectionInfo connectionInfo;
  const auto device {m_cp->device(deviceUUID)};
  const auto id {QString::number(device.connectionID())};

  QList<CControlPoint::TArgValue> args;
  args.reserve(8);
  args << CControlPoint::TArgValue("ConnectionID", id);
  args << CControlPoint::TArgValue("RcsID", {});
  args << CControlPoint::TArgValue("AVTransportID", {});
  args << CControlPoint::TArgValue("ProtocolInfo", {});
  args << CControlPoint::TArgValue("PeerConnectionManager", {});
  args << CControlPoint::TArgValue("PeerConnectionID", {});
  args << CControlPoint::TArgValue("Direction", {});
  args << CControlPoint::TArgValue("Status", {});

  auto actionInfo {
      m_cp->invokeAction(deviceUUID, "GetCurrentConnectionInfo", args)};
  if (actionInfo.succeeded()) {
    connectionInfo.setRcsID(args[1].second.toInt());
    connectionInfo.setAVTransportID(args[2].second.toInt());
    connectionInfo.setProtocolInfo(args[3].second);
    connectionInfo.setPeerConnectionManager(args[4].second);
    connectionInfo.setPeerConnectionID(args[5].second.toInt());
    connectionInfo.setDirection(args[6].second);
    connectionInfo.setStatus(args[7].second);
  }

  return connectionInfo;
}

}  // namespace QtUPnP
