#ifndef ACTION_HPP
#define ACTION_HPP

#include <QMap>
#include <QSharedDataPointer>
#include <unordered_map>

#include "argument.hpp"
#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{
/*! The arguments map.
 * \param QString: Argument name.
 * \param CArgument: The argument.
 */
using TMArguments = std::unordered_map<QString, CArgument>;

/*! Forward CAction data. */
struct SActionData;

/*! \brief The CAction class holds information about an UPnP action.
 *
 * It contains the map of arguments.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CAction
{
public:
  /*! Default constructor. */
  CAction();

  /*! Copy constructor. */
  CAction(CAction const& rhs);

  /*! Equal operator. */
  CAction& operator=(CAction const& other);

  /*! Destructor. */
  ~CAction();

  /*! Sets the argument map. */
  void setArguments(TMArguments const& args);

  /*! Return the arguments map as a constant reference. */
  [[nodiscard]] auto arguments() const -> TMArguments;

  /*! Return the arguments map as a constant reference. */
  [[nodiscard]] auto arguments() -> TMArguments&;

private:
  QSharedDataPointer<SActionData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // ACTION_HPP
