#ifndef BROWSE_REPLY_HPP
#define BROWSE_REPLY_HPP

#include <QSharedDataPointer>

#include "didlitem.hpp"
#include "qtUPnP/qtUPnP_export.hpp"

namespace QtUPnP
{

/*! Forward declaration of CBrowseReply data. */
struct SBrowseReplyData;

/*! \brief The CBrowseReply class holds information about the response of Browse
 * and Search action.
 *
 * The class CAVTransport is a wrapper that manages the
 * urn:schemas-upnp-org:service:AVTransport:1 service actions. From standard C++
 * aguments, the member functions invoke the corresponding action and return the
 * results in QtUPnP classes. See
 * http://upnp.org/specs/av/UPnP-av-ContentDirectory-Service.pdf.
 *
 * \remark Use implicit Sharing QT technology.
 */
class QTUPNP_EXPORT CBrowseReply
{
public:
  /*! Sort direction. */
  enum ESortDir
  {
    Ascending,
    Descending
  };

  /*! Default constructor. */
  CBrowseReply();

  /*! Copy constructor. */
  CBrowseReply(CBrowseReply const& other);

  /*! Equal operator. */
  CBrowseReply& operator=(CBrowseReply const& other);

  /*! Destructor. */
  ~CBrowseReply();

  /*! Sets the returnd number of CIdlItem. */
  void setNumberReturned(unsigned numberReturned);

  /*! Sets the total matches number of CIdlItem. */
  void setTotalMatches(unsigned totalMatches);

  /*! Sets the update identifier. */
  void setUpdateID(unsigned updateID);

  /*! Sets the list of CDidlItems. */
  void setItems(QList<CDidlItem> const& items);

  /*! Returns the returnd number of CIdlItem. */
  unsigned numberReturned() const;

  /*! Returns the total matches number of CIdlItem. */
  unsigned totalMatches() const;

  /*! Returns the update identifier. */
  unsigned updateID() const;

  /*! Returns the list of CDidlItems as a const reference. */
  QList<CDidlItem> const& items() const;

  /*! Returns the list of CDidlItems as a reference. */
  QList<CDidlItem>& items();

  /*! Add the other content at the content of this. */
  CBrowseReply& operator+=(CBrowseReply const& other);

  /*! Sorts the item list by criteria.
   * \param criteria: The criteria is the CDidlElem name or property.
   * To sort by CDidlElem name, the criteria must be the complet name (e.g. to
   * sort by title, the criteria is "dc:title"). To sort by property, the
   * criteria must have the form "name@property". \param dir: ascending or
   * descending see ESortDir. E.g. To sort by duration, the criteria is
   * "res@duration".
   */
  void sort(const QString& criteria, ESortDir dir = Ascending);

  /*! returns the sort capabilities. It is the list of criterias for sort
   * function.
   * \param reply: Detect the sort capabilities from CBrowseReply content.
   * \param sameContent: True assumes all CDidlElem contents are identical. In
   * this case, the function use only the first CDidlItem.
   */
  static QStringList sortCapabilities(CBrowseReply const& reply,
                                      bool sameContent = true);

  /*! returns the sort capabilities. It is the list of criterias for sort
   * function.
   * \param sameContent: True assumes all CDidlElem contents are identical. In
   * this case, the function use all CDidlItem elems.
   */
  QStringList sortCapabilities(bool sameContent = true) const;

  /*! Sorts the item list by criteria.
   * \param reply: CBrowseReply to sort.
   * \param criteria: The criteria is the CDidlElem name or property.
   * To sort by CDidlElem title, the criteria must be the complet title (e.g. to
   * sort by title, the criteria is "dc:title"). To sort by property, the
   * criteria must have the form "name@property". E.g. To sort by duration, the
   * criteria is "res@duration".
   * \param dir: Ascending or Descending see ESortDir.
   */
  static void sort(CBrowseReply& reply,
                   const QString& criteria,
                   ESortDir dir = Ascending);

  /*! Returns the CDidlItem list that match exactly or approximately text.
   * This function search the best corresponds between a text and a title list
   * and returns the list sorted by the better match.
   * \param text: Text to search.
   * \param commonPrefixLength; Winkler common prefix see jaroWinklerDistance in
   * helper.hpp.
   * \param returned: Max number of CDidlItem return. If returned <=
   * 0 or > items size, returnd whill be items size.
   * \return The list of sorted CDidlItem.
   */
  QList<CDidlItem> search(const QString& text,
                          int returned = -1,
                          int commonPrefixLength = -1) const;

  /*! Returns the CDidlItem list that match exactly or approximately text.
   * This function search the best corresponds between a text and a title list
   * and returns the list sorted by the better match. \param items: Search in
   * items by title. \param text: Text to search. \param commonPrefixLength;
   * Winkler common prefix see jaroWinklerDistance in helper.hpp. \param
   * returned: Max number of CDidlItem return. If returned <= 0 or > items size,
   * returnd whill be items size. \return The list of sorted CDidlItem.
   */
  static QList<CDidlItem> search(QList<CDidlItem> const& items,
                                 QString text,
                                 int returned = -1,
                                 int commonPrefixLength = -1);

  /*! Converts the content of this to a QStringList. */
  QStringList dump();

  /*! Write the content to QDebug. */
  void debug();

private:
  QSharedDataPointer<SBrowseReplyData> m_d;  //!< Shared data pointer.
};

}  // namespace QtUPnP

#endif  // BROWSE_REPLY_HPP
