#include <gtest/gtest.h>

#include "helper.hpp"

TEST(test, test3)
{
  ASSERT_EQ(QtUPnP::libraryName(), "QtUPnP");
  ASSERT_EQ(QtUPnP::libraryVersion(), "1.1.4");
}
