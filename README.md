# QtUPnP

QtUPnP framework is a C++ framework, based on QT5, to build easily an UPnP control point.
It focuses on the UPnP/AV standards.
For this, a set of classes are created to handle easily media servers and media renderers and to hide the UPnP protocol.
Of course UPnP protocol (UPnP/AV use UPnP) is implemented and your can handle every compatible devices.

Build this framework and the test program need to install QT5.
QtUPnP has been coded from:

- Windows
  - The build has been tested from and Qt 5.12.3 (`mingw73_32` and `mingw73_64`).
  - Do not forget to add to the path (global or QtCreator) the access to `qtupnp.dll` or change qtupnp library by a static library.

- Linux
  - The build has been tested Qt 5.12.3.
  - A 64 bits is built.
  - It has been tested on Ubuntu 18.04 under VirtualBox and Ubuntu 18.04 native.
  - To build QtUPnP without QtCreator, see linux-build folder.

- MacOS Mojave 10.14
  - The build has been tested from Qt 5.12.3 with Clang compiler.
  
- Raspbian
  - QtUPnP can be built using QT5.7 available in standard packages.
  - To build QtUPnP install QT5.7 and QtCreator on your Raspberry PI3.
    - `sudo apt-get install qt5-default`
    - `sudo apt-get install qtcreator`
  - It has been tested under Raspbian Stretch with desktop on a Raspberry PI3.

## Building and installing

See the [BUILDING](BUILDING.md) document.

## Contributing

See the [CONTRIBUTING](CONTRIBUTING.md) document.

## Remarks

- The framework use only QT API.
- The folder upnp contains the framework to encapsulate the UPnP protocol. By default, it is a dll for Windows and a static library for all other systems.

## chupnp

![chupnp screenshot](readme-images/chupnp.png)

chupnp is an application for developers to test the framework.

The main functionalities are:

- Discover devices
- Choose devices.
- Invoke the actions.
- and see the detailed results.
- ...
  