if(PROJECT_IS_TOP_LEVEL)
  set(CMAKE_INSTALL_INCLUDEDIR
      include/qtUPnP
      CACHE PATH "")
endif()

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# find_package(<package>) call for consumers to find this project
set(package qtUPnP)

install(
  DIRECTORY include/ "${PROJECT_BINARY_DIR}/export/"
  DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
  COMPONENT qtUPnP_Development)

install(
  TARGETS QtUPnP_lib QtAES
  EXPORT qtUPnPTargets
  RUNTIME #
          COMPONENT qtUPnP_Runtime
  LIBRARY #
          COMPONENT qtUPnP_Runtime NAMELINK_COMPONENT qtUPnP_Development
  ARCHIVE #
          COMPONENT qtUPnP_Development
  INCLUDES #
  DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")

write_basic_package_version_file("${package}ConfigVersion.cmake"
                                 COMPATIBILITY SameMajorVersion)

# Allow package maintainers to freely override the path for the configs
set(qtUPnP_INSTALL_CMAKEDIR
    "${CMAKE_INSTALL_LIBDIR}/cmake/${package}"
    CACHE PATH "CMake package config location relative to the install prefix")
mark_as_advanced(qtUPnP_INSTALL_CMAKEDIR)

install(
  FILES cmake/install-config.cmake
  DESTINATION "${qtUPnP_INSTALL_CMAKEDIR}"
  RENAME "${package}Config.cmake"
  COMPONENT qtUPnP_Development)

install(
  FILES "${PROJECT_BINARY_DIR}/${package}ConfigVersion.cmake"
  DESTINATION "${qtUPnP_INSTALL_CMAKEDIR}"
  COMPONENT qtUPnP_Development)

install(
  EXPORT qtUPnPTargets
  NAMESPACE qtUPnP::
  DESTINATION "${qtUPnP_INSTALL_CMAKEDIR}"
  COMPONENT qtUPnP_Development)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
