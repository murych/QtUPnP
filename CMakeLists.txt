cmake_minimum_required(VERSION 3.14)

include(cmake/prelude.cmake)

project(
  QtUPnP
  VERSION 1.1.4
  DESCRIPTION "A simple framework to build UPnP control point"
  HOMEPAGE_URL "https://gitlab.com/murych/QtUPnP"
  LANGUAGES CXX)

include(cmake/project-is-top-level.cmake)
include(cmake/variables.cmake)
include(GenerateExportHeader)
include(FetchContent)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(
  Qt5
  COMPONENTS Core Network Xml Gui Widgets
  REQUIRED)

set(CMAKE_TLS_VERIFY true)
FetchContent_Declare(
  Qt-AES
  GIT_REPOSITORY https://github.com/bricke/Qt-AES.git
  GIT_TAG 1.2)
FetchContent_MakeAvailable(Qt-AES)

# ---- Declare library ----

add_library(
  QtUPnP_lib
  source/action.cpp
  source/action.hpp
  source/actioninfo.cpp
  source/actioninfo.hpp
  source/actionmanager.cpp
  source/actionmanager.hpp
  source/argument.cpp
  source/argument.hpp
  source/avcontrol.cpp
  source/avcontrol.hpp
  source/avtransport.cpp
  source/avtransport.hpp
  source/browsereply.cpp
  source/browsereply.hpp
  source/connectioninfo.cpp
  source/connectioninfo.hpp
  source/connectionmanager.cpp
  source/connectionmanager.hpp
  source/contentdirectory.cpp
  source/contentdirectory.hpp
  source/control.cpp
  source/control.hpp
  source/controlpoint.cpp
  source/controlpoint.hpp
  source/datacaller.cpp
  source/datacaller.hpp
  source/device.cpp
  source/device.hpp
  source/devicecaps.cpp
  source/devicecaps.hpp
  source/devicemap.cpp
  source/devicemap.hpp
  source/devicepixmap.cpp
  source/devicepixmap.hpp
  source/didlitem.cpp
  source/didlitem.hpp
  source/didlitem_playlist.cpp
  source/dump.cpp
  source/dump.hpp
  source/eventingmanager.cpp
  source/eventingmanager.hpp
  source/helper.cpp
  source/helper.hpp
  source/httpparser.cpp
  source/httpparser.hpp
  source/httpserver.cpp
  source/httpserver.hpp
  source/initialdiscovery.cpp
  source/initialdiscovery.hpp
  source/mediainfo.cpp
  source/mediainfo.hpp
  source/multicastsocket.cpp
  source/multicastsocket.hpp
  source/oauth2.cpp
  source/oauth2.hpp
  source/pixmapcache.cpp
  source/pixmapcache.hpp
  source/plugin.cpp
  source/plugin.hpp
  source/positioninfo.cpp
  source/positioninfo.hpp
  source/renderingcontrol.cpp
  source/renderingcontrol.hpp
  source/service.cpp
  source/service.hpp
  source/statevariable.cpp
  source/statevariable.hpp
  source/status.hpp
  source/transportinfo.cpp
  source/transportinfo.hpp
  source/transportsettings.cpp
  source/transportsettings.hpp
  source/unicastsocket.cpp
  source/unicastsocket.hpp
  source/upnpsocket.cpp
  source/upnpsocket.hpp
  source/waitingloop.cpp
  source/waitingloop.hpp
  source/xmlh.cpp
  source/xmlh.hpp
  source/xmlhaction.cpp
  source/xmlhaction.hpp
  source/xmlhdevice.cpp
  source/xmlhdevice.hpp
  source/xmlhdidllite.cpp
  source/xmlhdidllite.hpp
  source/xmlhevent.cpp
  source/xmlhevent.hpp
  source/xmlhservice.cpp
  source/xmlhservice.hpp)

add_library(qtUPnP::qtUPnP ALIAS QtUPnP_lib)

generate_export_header(
  QtUPnP_lib
  BASE_NAME
  qtUPnP
  EXPORT_FILE_NAME
  export/qtUPnP/qtUPnP_export.hpp
  CUSTOM_CONTENT_FROM_VARIABLE
  pragma_suppress_c4251)

if(NOT BUILD_SHARED_LIBS)
  target_compile_definitions(QtUPnP_lib PUBLIC QTUPNP_STATIC_DEFINE)
endif()

set_target_properties(
  QtUPnP_lib
  PROPERTIES CXX_VISIBILITY_PRESET hidden
             VISIBILITY_INLINES_HIDDEN YES
             VERSION "${PROJECT_VERSION}"
             SOVERSION "${PROJECT_VERSION_MAJOR}"
             EXPORT_NAME qtUPnP
             OUTPUT_NAME qtUPnP)

target_include_directories(
  QtUPnP_lib ${warning_guard}
  PUBLIC "$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/source>")

target_include_directories(
  QtUPnP_lib SYSTEM PUBLIC "$<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/export>")

target_link_libraries(QtUPnP_lib PRIVATE Qt5::Gui Qt5::Core Qt5::Network
                                         Qt5::Xml)
target_link_libraries(QtUPnP_lib PRIVATE QtAES)
target_compile_features(QtUPnP_lib PUBLIC cxx_std_20)

# ---- Install rules ----

if(NOT CMAKE_SKIP_INSTALL_RULES)
  include(cmake/install-rules.cmake)
endif()

# ---- Examples ----

if(PROJECT_IS_TOP_LEVEL)
  option(BUILD_EXAMPLES "Build examples tree." "${qtUPnP_DEVELOPER_MODE}")
  if(BUILD_EXAMPLES)
    add_subdirectory(example)
  endif()
endif()

# ---- Developer mode ----

if(NOT qtUPnP_DEVELOPER_MODE)
  return()
elseif(NOT PROJECT_IS_TOP_LEVEL)
  message(AUTHOR_WARNING "Developer mode is intended for developers of qtUPnP")
endif()

include(cmake/dev-mode.cmake)
