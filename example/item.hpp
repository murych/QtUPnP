#ifndef ITEM_HPP
#define ITEM_HPP

#include <QTreeWidgetItem>

#include "didlitem.hpp"

enum EItemType
{
  NoType = QTreeWidgetItem::Type,
  BrowseType = QTreeWidgetItem::UserType,
  SearchType,
  ActionType,
};

class CItem : public QTreeWidgetItem
{
public:
  CItem(QTreeWidgetItem* item, QtUPnP::CDidlItem const& didl, EItemType type);

  auto didl() const { return m_didl; }

private:
  QtUPnP::CDidlItem m_didl;
};

#endif  // ITEM_HPP
