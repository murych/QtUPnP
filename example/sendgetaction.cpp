#include <QRegularExpression>

#include "avcontrol.hpp"
#include "helper.hpp"
#include "mainwindow.hpp"

static void addArg(QTreeWidgetItem* item, const QString& name, int value)
{
  new QTreeWidgetItem {item, QStringList(name + '=' + QString::number(value))};
}

static void addArg(QTreeWidgetItem* item, const QString& name, unsigned value)
{
  new QTreeWidgetItem {item, QStringList(name + '=' + QString::number(value))};
}

static void addArg(QTreeWidgetItem* item, const QString& name, bool value)
{
  new QTreeWidgetItem {item,
                       QStringList(name + '=' + (value ? "true" : "false"))};
}

static void addArg(QTreeWidgetItem* item,
                   const QString& name,
                   const QString& value)
{
  new QTreeWidgetItem {item, QStringList(name + '=' + value)};
}

static void addArg(QTreeWidgetItem* item,
                   const QString& name,
                   QStringList const& values)
{
  item = new QTreeWidgetItem {item, QStringList(name)};
  std::ranges::for_each(values,
                        [item](const auto& value) {
                          new QTreeWidgetItem {item, QStringList(value)};
                        });
}

void CMainWindow::sendGetAction(QTreeWidgetItem* item)
{
  if (item == nullptr) {
    return;
  }

  clearError(item);
  removeActionArgs(item);
  const QRegularExpression re {" \\(\\d+ms\\)"};
  const QString name {item->text(0).remove(re)};

  auto postRequestActions {[this](QTreeWidgetItem* item)
                           {
                             applyError(item);
                             addElapsedTime(item);
                             setGetActionItemColor(item);
                           }};

  if (name == "GetMute") {
    auto state {
        m_ctl.renderingControl().getMute(m_deviceUUID)};  // Get mute state.
    addArg(item, "Mute", state);
    postRequestActions(item);
    m_current.m_mute = state;
    return;
  }
  if (name == "GetVolume") {
    const auto volume {
        m_ctl.renderingControl().getVolume(m_deviceUUID)};  // Get volume.
    addArg(item, "Volume", volume);
    postRequestActions(item);
    m_current.m_vol = volume;
    return;
  }
  if (name == "ListPresets") {
    m_current.m_presets = m_ctl.renderingControl().getListPresets(m_deviceUUID);
    addArg(item, "Presets", m_current.m_presets);
    postRequestActions(item);
    return;
  }
  if (name == "GetCurrentConnectionIDs") {
    const auto replies {m_ctl.connectionManager().getCurrentConnectionIDs(
        m_deviceUUID)};  // Get connection IDs. Must be "0".
    addArg(item, "Current connection ID", replies);
    postRequestActions(item);
    return;
  }
  if (name.startsWith("GetProtocolInfo")) {
    const auto replies {m_ctl.connectionManager().getProtocolInfos(
        m_deviceUUID)};  // Get the mime types supported by the renderer.
    if (replies.size() > 1) {
      addArg(item, "Source", replies.at(0));
      addArg(item, "Sink", replies.at(1));
    }
    postRequestActions(item);
    return;
  }
  if (name == "GetCurrentTransportActions") {
    const auto replies {m_ctl.avTransport().getCurrentTransportActions(
        m_deviceUUID)};  // Get the valid current actions.
    addArg(item, "Actions", replies);
    postRequestActions(item);
    return;
  }
  if (name == "GetMediaInfo") {
    const auto info {
        m_ctl.avTransport().getMediaInfo(m_deviceUUID)};  // Get the media info.
    addArg(item, "Nr tracks", info.nrTracks());

    const auto duration {info.mediaDuration()};
    m_current.m_duration.clear();
    if (timeMs(duration) != 0) {
      m_current.m_duration = duration;
    }
    addArg(item, "Duration", duration);

    m_current.m_avTransportURI = info.currentURI();
    addArg(item, "Current uri", m_current.m_avTransportURI);
    addArg(item, "Current uri metadata", info.currentURIMetaData());

    m_current.m_nextAVTransportURI = info.nextURI();
    addArg(item, "Next uri", m_current.m_nextAVTransportURI);
    addArg(item, "Next uri metadata", info.nextURIMetaData());

    addArg(item, "Play Medium", info.playMedium());
    addArg(item, "Record Medium", info.recordMedium());
    addArg(item, "Write status", info.writeStatus());

    postRequestActions(item);
    return;
  }
  if (name == "GetPositionInfo") {
    const auto info {m_ctl.avTransport().getPositionInfo(
        m_deviceUUID)};  // Get the position info for the current transport.
    addArg(item, "Track number", info.track());
    addArg(item, "Track duration", info.trackDuration());
    addArg(item, "Track metadata", info.trackMetaData());
    addArg(item, "Track uri", info.trackURI());
    addArg(item, "Track relative time", info.relTime());
    addArg(item, "Track absolute time", info.absTime());
    addArg(item, "Track relative count", info.relCount());
    addArg(item, "Track absolute count", info.absCount());

    postRequestActions(item);

    if (timeMs(info.trackDuration()) != 0) {
      m_current.m_duration = info.trackDuration();
    }

    if (timeMs(info.relTime()) != 0) {
      m_current.m_relTime = info.relTime();
    }
    return;
  }
  if (name == "GetTransportInfo") {
    // Get the transport infos PAUSE_PLAYBACK, STOP, PLAY...
    const auto info {m_ctl.avTransport().getTransportInfo(m_deviceUUID)};
    addArg(item, "Current transport state", info.currentTransportState());
    addArg(item, "Current transport status", info.currentTransportStatus());
    addArg(item, "Current speed", info.currentSpeed());
    postRequestActions(item);
    m_current.m_transportInfo = info.currentTransportState();
    return;
  }
  if (name == "GetTransportSettings") {
    // Get the travsport settings (NORMAL, SUFFLE...).
    const auto settings {
        m_ctl.avTransport().getTransportSettings(m_deviceUUID)};
    addArg(item, "PlayMode", settings.playMode());
    addArg(item, "RecPlayMode", settings.recQualityMode());
    postRequestActions(item);
    m_current.m_transportSettings = settings.playMode();
    return;
  }
  if (name == "GetDeviceCapabilities") {
    const auto caps {m_ctl.avTransport().getDeviceCaps(m_deviceUUID)};
    addArg(item, "Play medias", caps.playMedias());
    addArg(item, "Record medias", caps.recMedias());
    addArg(item, "Record quality modes", caps.recQualityModes());
    postRequestActions(item);
    return;
  }
  if (name == "GetSearchCapabilities") {
    m_current.m_searchCaps =
        m_ctl.contentDirectory().getSearchCaps(m_deviceUUID);
    addArg(item, "Capabilities", m_current.m_searchCaps);
    postRequestActions(item);
    return;
  }
  if (name == "GetSortCapabilities") {
    const auto caps {m_ctl.contentDirectory().getSortCaps(m_deviceUUID)};
    addArg(item, "Capabilities", caps);
    postRequestActions(item);
    return;
  }
  if (name == "GetSystemUpdateID") {
    const auto id {m_ctl.contentDirectory().getSystemUpdateID(m_deviceUUID)};
    addArg(item, "ID", id);
    postRequestActions(item);
    return;
  }
  if (name == "GetCurrentConnectionInfo") {
    const auto info {
        m_ctl.connectionManager().getCurrentConnectionInfo(m_deviceUUID)};
    addArg(item, "RcsID", info.rcsID());
    addArg(item, "AVTransportID", info.avTransportID());
    addArg(item, "ProtocolInfo", info.protocolInfo());
    addArg(item, "PeerConnectionManager", info.peerConnectionManager());
    addArg(item, "PeerConnectionID", info.peerConnectionID());
    addArg(item, "Direction", info.direction());
    addArg(item, "Status", info.status());
    postRequestActions(item);
    return;
  }
  if (name == "GetBrightness") {
    const auto value {m_ctl.renderingControl().getBrightness(m_deviceUUID)};
    addArg(item, "Brightness", value);
    postRequestActions(item);
    m_current.m_brightness = value;
    return;
  }
  if (name == "GetContrast") {
    const auto value {m_ctl.renderingControl().getContrast(m_deviceUUID)};
    addArg(item, "Contrast", value);
    postRequestActions(item);
    m_current.m_contrast = value;
    return;
  }
  if (name == "GetSharpness") {
    const auto value {m_ctl.renderingControl().getSharpness(m_deviceUUID)};
    addArg(item, "Sharpness", value);
    postRequestActions(item);
    m_current.m_sharpness = value;
    return;
  }
  if (name == "GetRedVideoGain") {
    const auto value {m_ctl.renderingControl().getRedVideoGain(m_deviceUUID)};
    addArg(item, "RedVideoGain", value);
    postRequestActions(item);
    m_current.m_redVideoGain = value;
    return;
  }
  if (name == "GetRedVideoBlackLevel") {
    const auto value {
        m_ctl.renderingControl().getRedVideoBlackLevel(m_deviceUUID)};
    addArg(item, "RedVideoBlackLevel", value);
    postRequestActions(item);
    m_current.m_redVideoBlackLevel = value;
    return;
  }
  if (name == "GetGreenVideoGain") {
    const auto value {m_ctl.renderingControl().getGreenVideoGain(m_deviceUUID)};
    addArg(item, "GreenVideoGain", value);
    postRequestActions(item);
    m_current.m_greenVideoGain = value;
    return;
  }
  if (name == "GetGreenVideoBlackLevel") {
    const auto value {
        m_ctl.renderingControl().getGreenVideoBlackLevel(m_deviceUUID)};
    addArg(item, "GreenVideoBlackLevel", value);
    postRequestActions(item);
    m_current.m_greenVideoBlackLevel = value;
    return;
  }
  if (name == "GetBlueVideoGain") {
    const auto value {m_ctl.renderingControl().getBlueVideoGain(m_deviceUUID)};
    addArg(item, "BlueVideoGain", value);
    postRequestActions(item);
    m_current.m_blueVideoGain = value;
    return;
  }
  if (name == "GetBlueVideoBlackLevel") {
    const auto value {
        m_ctl.renderingControl().getBlueVideoBlackLevel(m_deviceUUID)};
    addArg(item, "BlueVideoBlackLevel", value);
    postRequestActions(item);
    m_current.m_blueVideoBlackLevel = value;
    return;
  }
  if (name == "GetColorTemperature") {
    const auto value {
        m_ctl.renderingControl().getColorTemperature(m_deviceUUID)};
    addArg(item, "ColorTemperature", value);
    postRequestActions(item);
    m_current.m_colorTemperature = value;
    return;
  }
  if (name == "GetHorizontalKeystone") {
    const auto value {
        m_ctl.renderingControl().getHorizontalKeystone(m_deviceUUID)};
    addArg(item, "HorizontalKeystone", value);
    postRequestActions(item);
    m_current.m_horizontalKeystone = value;
    return;
  }
  if (name == "GetVerticalKeystone") {
    const auto value {
        m_ctl.renderingControl().getVerticalKeystone(m_deviceUUID)};
    addArg(item, "VerticalKeystone", value);
    postRequestActions(item);
    m_current.m_verticalKeystone = value;
    return;
  }
  if (name == "GetVolumeDB") {
    const auto value {m_ctl.renderingControl().getVolumeDB(m_deviceUUID)};
    addArg(item, "VolumeDB", value);
    postRequestActions(item);
    m_current.m_volDB = value;
    return;
  }
  if (name == "GetVolumeDBRange") {
    const auto minmax {m_ctl.renderingControl().getVolumeDBRange(m_deviceUUID)};
    addArg(item, "Min", minmax.first);
    addArg(item, "Max", minmax.second);
    postRequestActions(item);
    m_current.m_volMinDB = minmax.first;
    m_current.m_volMaxDB = minmax.second;
    return;
  }
  if (name == "GetLoudness") {
    const auto state {
        m_ctl.renderingControl().getLoudness(m_deviceUUID)};  // Get mute state.
    addArg(item, "loudness", state);
    postRequestActions(item);
    m_current.m_loudness = state;
    return;
  }
}

void CMainWindow::updateGetAction(const QString& name)
{
  const auto actions {m_actionLinks.at(name)};
  std::ranges::for_each(actions,
                        [this](const auto& action)
                        {
                          auto* item {findActionItem(action)};
                          if (item == nullptr) {
                            return;
                          }
                          sendGetAction(item);
                          item->setExpanded(true);
                        });
}
