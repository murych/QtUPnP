#ifndef HELPER_HPP
#define HELPER_HPP

#include <QString>

class QTime;
class QTreeWidgetItem;
class QColor;
class QTextStream;

[[nodiscard]] auto isHandled(const QString& name) -> bool;
void addElapsedTime(QTreeWidgetItem* item);
void setGetActionItemColor(QTreeWidgetItem* item);
void setItemColor(QTreeWidgetItem* item, const QColor& color);
[[nodiscard]] auto timeMs(const QTime& time) -> int;
[[nodiscard]] auto timeMs(const QString& time) -> int;
void enumerateChildren(QTextStream& s, QTreeWidgetItem* item);
void addTotalTime(QTreeWidgetItem* item, int ms);

#endif  // HELPER_HPP
