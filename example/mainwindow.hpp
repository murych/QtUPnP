#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QBrush>
#include <QMainWindow>
#include <QMap>
#include <QStringList>
#include <QTimer>
#include <QTreeWidgetItem>

#include "avcontrol.hpp"
#include "item.hpp"
#include "pixmapcache.hpp"

namespace Ui
{
class CMainWindow;
}

namespace upnp
{
class CControlPoint;
};

class QProgressBar;

class CMainWindow : public QMainWindow
{
  Q_OBJECT

public:
  // To store some cuttent values.
  struct SCurrent
  {
    void reset()
    {
      m_vol = 0, m_volMin = 0, m_volMax = 0, m_volStep = 1;
      m_brightness = m_contrast = m_sharpness = m_redVideoGain =
          m_redVideoBlackLevel = m_greenVideoGain = m_greenVideoBlackLevel =
              m_blueVideoGain = m_blueVideoBlackLevel = m_colorTemperature =
                  m_horizontalKeystone = m_verticalKeystone = m_volDB = 0,
      m_volMinDB = 0, m_volMaxDB = 0;
      m_loudness = m_mute = false;
      m_avTransportURI.clear();
      m_nextAVTransportURI.clear();
      m_transportInfo.clear();
      m_transportSettings.clear();
      m_playModes.clear();
      m_searchCaps.clear();
      m_relTime.clear();
      m_duration.clear();
      m_criteria.clear();
      m_preset.clear();
      m_presets.clear();
    }

    int m_vol {0}, m_volMin {0}, m_volMax {0}, m_volStep {1};
    int m_brightness {0};
    int m_contrast {0};
    int m_sharpness {0};
    int m_redVideoGain {0};
    int m_redVideoBlackLevel {0};
    int m_greenVideoGain {0};
    int m_greenVideoBlackLevel {0};
    int m_blueVideoGain {0};
    int m_blueVideoBlackLevel {0};
    int m_colorTemperature {0};
    int m_horizontalKeystone {0};
    int m_verticalKeystone {0};
    int m_volDB {0}, m_volMinDB {0}, m_volMaxDB {0};
    bool m_loudness {false};
    bool m_mute {false};
    QString m_avTransportURI;
    QString m_nextAVTransportURI;
    QString m_transportInfo;
    QString m_transportSettings;
    QStringList m_playModes;
    QStringList m_searchCaps;
    QString m_relTime, m_duration;
    QString m_criteria;
    QString m_preset;
    QStringList m_presets;
  };

  explicit CMainWindow(QWidget* parent = nullptr);
  ~CMainWindow() override;

protected:
  // See Detailed Description of CControlPoint
  void closeEvent(QCloseEvent*) override;

protected slots:
  void newDevice(const QString& uuid);
  void lostDevice(const QString& uuid);
  void eventReady(QStringList const& emitter);
  void upnpError(int errorCode, const QString& errorString);
  void networkError(const QString& deviceUUID,
                    QNetworkReply::NetworkError errorCode,
                    const QString& errorString);
  void discoveryLaunched(char const* nt, int index, int count);
  void timeout();
  void on_m_rescan_triggered();
  void on_m_update_triggered();
  void on_m_export_triggered();
  void on_m_copyURI_triggered();
  void on_m_playlist_triggered();
  void on_m_search_triggered();
  void on_m_devices_itemDoubleClicked(QTreeWidgetItem* item, int column);
  void on_m_services_itemDoubleClicked(QTreeWidgetItem* item, int column);
  void on_m_services_itemClicked(QTreeWidgetItem* item, int column);

private:
  QTreeWidgetItem* insertItem(QTreeWidgetItem* parentItem,
                              QtUPnP::CDevice const& device);
  void createActionLinks();
  void hideProgressBar(bool hide);
  void loadServices(const QString& uuid);
  bool subscription(const QString& uuid);
  void sendGetAction(QTreeWidgetItem* item);
  void updateGetAction(const QString& name);
  QTreeWidgetItem* findActionItem(const QString& text);
  void removeActionArgs(QTreeWidgetItem* item);
  void applyError(QTreeWidgetItem* item);
  void clearError(QTreeWidgetItem* item);
  void browse(QTreeWidgetItem* item);
  void search(QTreeWidgetItem* item, const QString& criteria);
  void updateTree(QTreeWidgetItem* item,
                  QtUPnP::CBrowseReply const& reply,
                  EItemType type);
  void insertDidlElems(CItem* item);
  void saveDevices();
  void restoreDevices();

private:
  const std::unique_ptr<Ui::CMainWindow> ui {nullptr};

  //! Progress bar during discovery.
  const std::unique_ptr<QProgressBar> m_pb {nullptr};

  //! It is the CControlPoint.
  std::unique_ptr<QtUPnP::CControlPoint> m_cp {nullptr};

  //! AV manager action (just an orginazation).
  QtUPnP::CAVControl m_ctl;

  //! Use to start discovery and update progress bar during disconery.
  QTimer m_timer;

  //!< Discovery running.
  bool m_initRunning {true};

  //! The current device UUID.
  QString m_deviceUUID;

  //!< Links between actions. e.g setMute expand mute.
  std::unordered_map<QString, QStringList> m_actionLinks;

  //! Some current variable values.
  SCurrent m_current;

  //! Use to create playlist.
  QList<QtUPnP::CDidlItem> m_selectedTracks;

  //! Default QTreeWidgetItem color
  QBrush m_itemBrush;

  //! Version of this code.
  int m_version {100};

  //! Cache of pixmaps.
  QtUPnP::CPixmapCache m_pxmCache;

  //! Store the current devices for future used. In case of true SaveDevices is
  //! called in close event, and restoreDevices is call at start up.
  bool m_storeDevices {false};
};

#endif  // MAINWINDOW_HPP
