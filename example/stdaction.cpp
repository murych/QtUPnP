#include <QLabel>
#include <QTextEdit>

#include "stdaction.hpp"

#include "actioninfo.hpp"
#include "ui_stdaction.h"

using namespace QtUPnP;

CStdAction::CStdAction(CControlPoint* cp,
                       const QString& deviceUUID,
                       const QString& action,
                       QWidget* parent)
    : QDialog {parent}
    , ui {std::make_unique<Ui::CStdAction>()}
    , m_cp {cp}
    , m_deviceUUID {deviceUUID}
    , m_actionName {action}
{
  ui->setupUi(this);

  m_outLayout = new QGridLayout {ui->m_out};
  m_outLayout->setSizeConstraint(QLayout::SetMaximumSize);

  m_inLayout = new QGridLayout {ui->m_in};
  m_inLayout->setSizeConstraint(QLayout::SetMinimumSize);

  createWidgets(action);
}

CStdAction::~CStdAction() = default;

void CStdAction::createWidgets(const QString& actionName)
{
  const auto device {m_cp->device(m_deviceUUID)};
  const auto serviceIDs {device.serviceIDs(actionName)};
  if (!serviceIDs.isEmpty()) {
    m_serviceID = serviceIDs.first();
    if (serviceIDs.size() > 1) {
      // To do. Actually assume the action name is always different accross all
      // services.
    }

    const auto service {device.services().at(m_serviceID)};
    const auto action {service.actions().value(actionName)};
    const auto args {action.arguments()};
    auto row {0};

    std::for_each(
        args.cbegin(),
        args.cend(),
        [&](const auto& it)
        {
          const auto arg {it.second};
          const auto name {it.first};
          const auto stateVariableName {arg.relatedStateVariable()};
          const auto var {service.stateVariables().value(stateVariableName)};
          const auto stringType {var.type() == CStateVariable::String};
          const auto dir {arg.dir()};

          QWidget* parent;
          QGridLayout* layout;
          bool readOnly;
          if (dir == CArgument::In) {
            parent = ui->m_in;
            layout = m_inLayout;
            readOnly = false;
          } else {
            parent = ui->m_out;
            layout = m_outLayout;
            readOnly = true;
          }

          auto* lName {new QLabel {parent}};
          lName->setObjectName("name:" + name);
          lName->setText(name);
          layout->addWidget(lName, row, 0, Qt::AlignRight);

          auto* tValue {new QTextEdit {parent}};
          tValue->setObjectName(name);
          tValue->setReadOnly(readOnly);
          if (!stringType) {
            tValue->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
            tValue->setMaximumHeight(22);
          }

          layout->addWidget(tValue, row, 1);
          ++row;
        });
  }
}

void CStdAction::on_m_invoke_clicked()
{
  m_args.clear();
  QList<QTextEdit*> argWidgets;
  argWidgets << ui->m_in->findChildren<QTextEdit*>();
  const auto cInArgs {argWidgets.size()};
  argWidgets << ui->m_out->findChildren<QTextEdit*>();

  std::for_each(argWidgets.cbegin(),
                argWidgets.cend(),
                [this](auto* argWidget)
                {
                  CControlPoint::TArgValue av {argWidget->objectName(),
                                               argWidget->toPlainText()};
                  m_args << av;
                });

  const auto actionInfo {
      m_cp->invokeAction(m_deviceUUID, m_serviceID, m_actionName, m_args)};
  ui->m_response->setText(actionInfo.response());

  for (int iOutArg = cInArgs, cArgs = argWidgets.size(); iOutArg < cArgs;
       ++iOutArg)
  {
    const auto text {m_args.at(iOutArg).second};
    argWidgets[iOutArg]->setText(text);
  }
}

void CStdAction::on_m_close_clicked()
{
  accept();
}
