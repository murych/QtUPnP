#include "search.hpp"

#include "browsereply.hpp"
#include "ui_search.h"

using namespace QtUPnP;

CSearch::CSearch(const QList<CDidlItem>& items, QWidget* parent)
    : QDialog {parent}
    , ui {std::make_unique<Ui::CSearch>()}
    , m_items {items}
{
  ui->setupUi(this);
  std::ranges::for_each(items,
                        [this](const CDidlItem& item)
                        { ui->m_items->addItem(item.title()); });
}

CSearch::~CSearch() = default;

void CSearch::on_m_text_textChanged(const QString& text)
{
  ui->m_items->clear();

  const auto sortedItems {
      text.isEmpty() ? m_items : CBrowseReply::search(m_items, text, 30)};
  std::ranges::for_each(sortedItems,
                        [this](const CDidlItem& item)
                        { ui->m_items->addItem(item.title()); });

  ui->m_items->scrollToTop();
}
