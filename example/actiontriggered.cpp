#include <QClipboard>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QStandardPaths>
#include <QTreeWidget>

#include "helper.hpp"
#include "item.hpp"
#include "mainwindow.hpp"
#include "search.hpp"
#include "ui_mainwindow.h"

using namespace QtUPnP;

void CMainWindow::on_m_rescan_triggered()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  ui->m_tabWidget->setCurrentIndex(0);
  ui->m_services->clear();
  hideProgressBar(false);

  QElapsedTimer ti;
  ti.start();

  m_cp->discover();  // Launch the UPnP device discovery. This function can be
                     // called more than once.

  const auto ms {ti.elapsed()};
  const auto message {
      QString("Discovery time: %1 s").arg(static_cast<float>(ms) / 1000.0f)};
  statusBar()->showMessage(message);
  hideProgressBar(true);
  QApplication::restoreOverrideCursor();
}

void CMainWindow::on_m_update_triggered()
{
  loadServices(m_deviceUUID);
}

void CMainWindow::on_m_export_triggered()
{
  auto enumerateVariable {
      [](QTextStream& s, QTreeWidgetItem* item)
      {
        s << "<table border=\"1\" cellpadding=\"2px;\">";
        for (int iItem = 0; iItem < item->childCount(); ++iItem) {
          auto* varItem {item->child(iItem)};
          s << "<tr>";
          for (int iCol = 0; iCol < varItem->columnCount(); ++iCol) {
            const auto text {varItem->text(iCol)};
            s << "<td>" << text << "</td>";
          }
          s << "</tr>";
        }
        s << "</table>";
      }};

  const QStringList args {QApplication::arguments()};
  const QFileInfo fileInfo {args.at(0)};
  const QString exeBaseName {fileInfo.baseName()};

  const QDateTime date {QDateTime::currentDateTime()};
  const QStringList dirs {
      QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)};

  QString caption;
  QString fileName {QStringLiteral("%1_upnp_services_%2.htm")
                        .arg(exeBaseName, date.toString())};
  fileName.replace('/', '-');
  fileName.replace(':', '-');
  fileName.replace(' ', '_');

  const QString dir {QDir {dirs.first()}.absoluteFilePath(fileName)};
  fileName = QFileDialog::getSaveFileName(this, caption, dir);
  if (fileName.isEmpty()) {
    return;
  }
  QFile file {fileName};
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    return;
  }
  QTextStream s {&file};
  s << "<!doctype html><html><head><meta http-equiv=\"content-type\" "
       "content=\"text/html; "
       "charset=utf-8\"/><title>UPnP</title></head><body>";

  const auto v100 {m_version / 100};
  const auto v10 {(m_version - v100 * 100) / 10};
  const auto v1 {m_version - v100 * 100 - v10 * 10};
  const auto v {QStringLiteral("%1.%2.%3").arg(v100).arg(v10).arg(v1)};
  auto u {QStringLiteral("<h1 align=\"center\">%1 - %2 (%3)</h1>")
              .arg(tr("UPnP services"), exeBaseName, v)};
  s << u.toUtf8();

  u = QStringLiteral("<p><font size=\"4\"><b>%1</b></font>%2</p>")
          .arg(tr("Date: "), date.toString());
  s << u.toUtf8();

  s << "<ul>";
  for (int i = 0; i < ui->m_services->topLevelItemCount(); ++i) {
    auto* item {ui->m_services->topLevelItem(i)};
    s << "<li>" << item->text(0) << "</li>";

    for (int iChild = 0; iChild < item->childCount(); ++iChild) {
      auto* childItem {item->child(iChild)};
      s << "<ul>";
      const auto text {childItem->text(0)};
      s << "<li>" << text << "</li>";
      if (text == "Variables") {
        enumerateVariable(s, childItem);
      } else {
        enumerateChildren(s, childItem);
      }

      s << "</ul>";
    }
  }

  s << "</ul>";
  s << "</table></body></html>";
}

void CMainWindow::on_m_copyURI_triggered()
{
  const auto items {ui->m_services->selectedItems()};
  if (!items.isEmpty()) {
    on_m_services_itemClicked(items.first(), 0);
  }
}

void CMainWindow::on_m_playlist_triggered()
{
  m_selectedTracks.clear();
  const auto items {ui->m_services->selectedItems()};
  if (items.isEmpty()) {
    return;
  }

  m_selectedTracks.reserve(items.size());
  std::ranges::for_each(items,
                        [this](auto* item)
                        {
                          if (item->type() == BrowseType) {
                            const auto didl {static_cast<CItem*>(item)->didl()};
                            if (didl.type() == CDidlItem::MusicTrack) {
                              m_selectedTracks.push_back(didl);
                            }
                          }
                        });
}

void CMainWindow::on_m_search_triggered()
{
  const auto items {ui->m_services->selectedItems()};
  QList<CDidlItem> didlItems;
  didlItems.reserve(items.size());
  std::ranges::for_each(items,
                        [&didlItems](auto* item)
                        {
                          if (item->type() == BrowseType) {
                            const auto didl {static_cast<CItem*>(item)->didl()};
                            if (!didl.isContainer()) {
                              didlItems.push_back(didl);
                            }
                          }
                        });

  CSearch search {didlItems, this};
  search.exec();
}
