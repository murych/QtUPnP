#ifndef STDACTION_HPP
#define STDACTION_HPP

#include <QDialog>

#include "controlpoint.hpp"

namespace Ui
{
class CStdAction;
}

namespace QtUPnP
{
class CControlPoint;
}

class QGridLayout;

/*! \brief The dialog to enter in parameters, launch an action and display out
 * parameters. */
class CStdAction : public QDialog
{
  Q_OBJECT

public:
  explicit CStdAction(QtUPnP::CControlPoint* cp,
                      const QString& deviceUUID,
                      const QString& action,
                      QWidget* parent = nullptr);
  ~CStdAction() override;

protected slots:
  void on_m_invoke_clicked();
  void on_m_close_clicked();

private:
  void createWidgets(const QString& actionName);

private:
  const std::unique_ptr<Ui::CStdAction> ui {nullptr};
  QGridLayout *m_inLayout = nullptr, *m_outLayout = nullptr;
  QtUPnP::CControlPoint* m_cp = nullptr;
  QString m_deviceUUID;
  QString m_serviceID;
  QString m_actionName;
  QList<QtUPnP::CControlPoint::TArgValue> m_args;
};

#endif  // STDACTION_HPP
