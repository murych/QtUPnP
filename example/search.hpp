#ifndef SEARCH_HPP
#define SEARCH_HPP

#include <QDialog>

#include "didlitem.hpp"

namespace Ui
{
class CSearch;
}

class CSearch : public QDialog
{
  Q_OBJECT

public:
  explicit CSearch(const QList<QtUPnP::CDidlItem>& items,
                   QWidget* parent = nullptr);
  ~CSearch() override;

protected slots:
  void on_m_text_textChanged(const QString& text);

private:
  const std::unique_ptr<Ui::CSearch> ui {nullptr};
  QList<QtUPnP::CDidlItem> m_items;
};

#endif  // SEARCH_HPP
