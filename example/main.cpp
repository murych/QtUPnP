#include <QApplication>

#include "mainwindow.hpp"

int main(int argc, char* argv[])
{
  QApplication a {argc, argv};

  CMainWindow w;
  w.show();

  return QApplication::exec();
}
