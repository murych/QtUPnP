#include "item.hpp"

CItem::CItem(QTreeWidgetItem* item,
             QtUPnP::CDidlItem const& didl,
             EItemType type)
    : QTreeWidgetItem {item, type}
    , m_didl {didl}
{
  const auto elem {didl.value("dc:title")};
  const auto title {elem.value()};
  setText(0, title);
}
