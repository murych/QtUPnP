#include <QDir>
#include <QElapsedTimer>
#include <QFileInfo>
#include <QProgressBar>
#include <QStandardPaths>

#include "mainwindow.hpp"

#include "helper.hpp"
#include "ui_mainwindow.h"

using namespace QtUPnP;

CMainWindow::CMainWindow(QWidget* parent)
    : QMainWindow {parent}
    , ui {std::make_unique<Ui::CMainWindow>()}
    , m_pb {std::make_unique<QProgressBar>()}
    , m_cp {std::make_unique<QtUPnP::CControlPoint>()}
{
  ui->setupUi(this);

  m_pb->setMaximum(10);
  m_pb->setMaximumHeight(16);
  statusBar()->addPermanentWidget(m_pb.get());
  connect(&m_timer, &QTimer::timeout, this, &CMainWindow::timeout);

  m_cp->setExpandEmbeddedDevices();  // Put a copy of embedded devices in the
                                     // device map.
  connect(m_cp.get(),
          &QtUPnP::CControlPoint::searched,
          this,
          &CMainWindow::discoveryLaunched);  // Not essential. Can be skipped.
  connect(m_cp.get(),
          &QtUPnP::CControlPoint::eventReady,
          this,
          &CMainWindow::eventReady);
  connect(m_cp.get(),
          &QtUPnP::CControlPoint::upnpError,
          this,
          &CMainWindow::upnpError);
  connect(m_cp.get(),
          &QtUPnP::CControlPoint::networkError,
          this,
          &CMainWindow::networkError);
  connect(m_cp.get(),
          &QtUPnP::CControlPoint::newDevice,
          this,
          &CMainWindow::newDevice);
  connect(m_cp.get(),
          &QtUPnP::CControlPoint::lostDevice,
          this,
          &CMainWindow::lostDevice);

  m_ctl.setControlPoint(m_cp.get());
  ui->m_tabWidget->setCurrentIndex(0);
  m_itemBrush = ui->m_devices->topLevelItem(0)->foreground(0);
  createActionLinks();
  m_timer.start(500);  // Launch discovery when all widgets are polished.
}

CMainWindow::~CMainWindow() = default;

void CMainWindow::closeEvent(QCloseEvent*)
{
  saveDevices();
  m_cp->close();
}

void CMainWindow::hideProgressBar(bool hide)
{
  m_pb->setValue(0);
  m_pb->setHidden(hide);
}

QTreeWidgetItem* CMainWindow::findActionItem(const QString& text)
{
  QTreeWidgetItem* item {nullptr};
  QTreeWidgetItemIterator it {ui->m_services};
  while ((*it) != nullptr) {
    const QRegularExpression re {" \\(\\d+ms\\)"};
    const QString itemText {(*it)->text(0).remove(re)};
    if (itemText == text) {
      item = *it;
      break;
    }

    ++it;
  }

  return item;
}

void CMainWindow::createActionLinks()
{
  m_actionLinks.emplace("SetMute", QStringList {"GetMute"});
  m_actionLinks.emplace("SetVolume", QStringList {"GetVolume"});
  m_actionLinks.emplace(
      "SetVolumeDB",
      QStringList {"GetVolume", "GetVolumeDB", "GetVolumeDBRange"});
  m_actionLinks.emplace("SetAVTransportURI", QStringList {"GetMediaInfo"});
  m_actionLinks.emplace("PPS", QStringList {"GetTransportInfo"});
  m_actionLinks.emplace("SetPlayMode", QStringList {"GetTransportSettings"});
  m_actionLinks.emplace("Seek", QStringList {"GetPositionInfo"});
  m_actionLinks.emplace("PN", QStringList {"GetMediaInfo"});
  m_actionLinks.emplace("SelectPreset", QStringList {"ListPresets"});
}

void CMainWindow::removeActionArgs(QTreeWidgetItem* item)
{
  if (item == nullptr) {
    return;
  }
  while (item->childCount() != 0) {
    auto* itemChild {item->takeChild(0)};
    delete itemChild;
  }
}

void CMainWindow::applyError(QTreeWidgetItem* item)
{
  if (CActionManager::lastError().isEmpty()) {
    return;
  }
  setItemColor(item, QColor {250, 0, 0});
  statusBar()->showMessage(CActionManager::lastError());
}

void CMainWindow::clearError(QTreeWidgetItem* item)
{
  item->setForeground(0, m_itemBrush);
}

// Insert DIDL-Lite elements.
void CMainWindow::insertDidlElems(CItem* item)
{
  QMapIterator<QString, CDidlElem> ite {item->didl().elems()};
  while (ite.hasNext()) {
    ite.next();
    const auto elem {ite.value()};  // DIDL-Lite element.
    const auto key {ite.key()};  // Name.
    const auto value {elem.value()};  // Value.

    QString text {key + ":\"" + value + '\"'};
    auto* elemItem {new QTreeWidgetItem {item, QStringList(text)}};

    // Enumerate properties.
    const auto props {elem.props()};
    if (props.isEmpty()) {
      return;
    }
    QMapIterator<QString, QString> itp(props);
    while (itp.hasNext()) {
      itp.next();
      const auto name {itp.key()};  // Property name.
      const auto value {itp.value()};  // Property value.
      text = name + ":\"" + value + '\"';
      new QTreeWidgetItem(elemItem, QStringList(text));
    }
  }
}

void CMainWindow::updateTree(QTreeWidgetItem* item,
                             CBrowseReply const& reply,
                             EItemType type)
{
  QElapsedTimer ti;
  ti.start();

  const auto didlItems {reply.items()};
  const auto cItems {didlItems.size()};
  std::ranges::for_each(
      didlItems,
      [&](const CDidlItem& didlItem)
      {
        auto* itemChild {new CItem {item, didlItem, type}};
        const auto type {didlItem.type()};
        const auto uris {didlItem.albumArtURIs().isEmpty()
                                 && (type == CDidlItem::ImageItem
                                     || type == CDidlItem::Photo)
                             ? didlItem.uris()
                             : didlItem.albumArtURIs()};

        const auto thumbnail {!uris.isEmpty() ? uris.last() : QString {}};
        const QSize pixmapSize {16, 16};
        const QPixmap pixmap {
            [&]()
            {
              if (!thumbnail.isEmpty()) {
                const auto cached {m_pxmCache.search(thumbnail)};
                if (!cached.isNull()) {
                  return cached;
                }
                // not found in cache, get from server
                CDataCaller dc {m_cp->networkAccessManager()};
                const auto remote {dc.callData(thumbnail)};
                if (!remote.isNull()) {
                  return m_pxmCache.add(thumbnail, remote, pixmapSize);
                }
              }

              // No pixmap avalaible use generic pixmap.
              switch (type) {
                case CDidlItem::ImageItem:
                case CDidlItem::Photo:
                  return QIcon::fromTheme("image-x-generic").pixmap(pixmapSize);

                case CDidlItem::AudioItem:
                case CDidlItem::MusicTrack:
                case CDidlItem::AudioBroadcast:
                case CDidlItem::AudioBook:
                case CDidlItem::AudioProgram:
                  return QIcon::fromTheme("audio-x-generic").pixmap(pixmapSize);

                case CDidlItem::VideoItem:
                case CDidlItem::Movie:
                case CDidlItem::VideoBroadcast:
                case CDidlItem::MusicVideoClip:
                case CDidlItem::VideoProgram:
                  return QIcon::fromTheme("video-x-generic").pixmap(pixmapSize);

                case CDidlItem::Container:
                  return QIcon::fromTheme("folder-database").pixmap(pixmapSize);

                case CDidlItem::MusicAlbum:
                  return QIcon::fromTheme("folder-music").pixmap(pixmapSize);

                default:
                  return QIcon::fromTheme(CDidlItem::isContainer(type)
                                              ? "folder"
                                              : "unknown")
                      .pixmap(pixmapSize);
              }
            }()};

        if (!pixmap.isNull()) {
          itemChild->setIcon(0, pixmap);
        }

        ui->m_services->scrollToItem(itemChild);
        insertDidlElems(itemChild);
      });

  addElapsedTime(item);
}

void CMainWindow::browse(QTreeWidgetItem* item)
{
  const auto didlItem {static_cast<CItem*>(item)};
  const auto didl {didlItem->didl()};
  const auto id {didl.isEmpty() ? "0"
                                : didlItem->didl().value("container", "id")};
  if (id.isEmpty()) {
    return;
  }
  removeActionArgs(item);
  item->setExpanded(true);

  CContentDirectory cd {m_cp.get()};
  CBrowseReply reply {cd.browse(m_deviceUUID, id)};
  updateTree(item, reply, BrowseType);
}

void CMainWindow::search(QTreeWidgetItem* item, const QString& criteria)
{
  const auto didlItem {static_cast<CItem*>(item)};
  const auto didl {didlItem->didl()};
  const auto id {didl.isEmpty() ? "0"
                                : didlItem->didl().value("container", "id")};
  if (id.isEmpty()) {
    return;
  }
  removeActionArgs(item);
  item->setExpanded(true);
  CContentDirectory cd {m_cp.get()};
  CBrowseReply reply {cd.search(m_deviceUUID, id, criteria)};
  updateTree(item, reply, SearchType);
}

QTreeWidgetItem* CMainWindow::insertItem(QTreeWidgetItem* parentItem,
                                         const QtUPnP::CDevice& device)
{
  // Get device name. This is the friendly name if it exists, otherwise the name
  const auto name {device.name()};
  const auto uuid {device.uuid()};
  const auto host {device.url().host()};
  const auto port {QString::number(device.url().port())};
  const auto text {
      QStringLiteral("%1 [%2:%3] (%4)").arg(name, host, port, uuid)};

  auto* item {new QTreeWidgetItem {parentItem, QStringList(text)}};

  // Retreive the pixmap attached at the device.
  auto pxmBytes {device.pixmapBytes(m_cp->networkAccessManager())};
  if (!pxmBytes.isEmpty()) {
    QPixmap pxm;
    // CDevice::pixmapBytes return the content of image.
    pxm.loadFromData(pxmBytes);
    // Set item icon.
    item->setIcon(0, pxm);
  }

  // Attach the uuid at the item for future use.
  item->setData(0, Qt::UserRole, device.uuid());

  const auto subDevices {device.subDevices()};
  std::ranges::for_each(subDevices, [&](auto& it) { insertItem(item, it); });

  return item;
}

void CMainWindow::saveDevices()
{
  if (!m_storeDevices) {
    return;
  }
  const auto devices {m_cp->devicesFinding()};
  const auto dirs {
      QStandardPaths::standardLocations(QStandardPaths::TempLocation)};
  QFileInfo fi {QDir {dirs.first()}, "QtUPnPDevices"};
  QFile file {fi.absoluteFilePath()};
  file.open(QIODevice::WriteOnly | QIODevice::Text);
  QTextStream ts {&file};
  std::ranges::for_each(
      devices,
      [&ts](const auto& device)
      { ts << device.first << ',' << device.second.toString() << '\n'; });

  file.close();
}

void CMainWindow::restoreDevices()
{
  if (!m_storeDevices) {
    return;
  }
  QList<QPair<QString, QUrl>> devices;
  const auto dirs {
      QStandardPaths::standardLocations(QStandardPaths::TempLocation)};
  QFileInfo fi {QDir {dirs.first()}, "QtUPnPDevices"};
  QFile file {fi.absoluteFilePath()};
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  QTextStream ts {&file};
  for (;;) {
    const auto line {ts.readLine()};
    const auto data {line.split(',')};
    if (data.size() == 2) {
      devices.append({data.at(0), QUrl {data.at(1)}});
    } else {
      break;
    }
  }

  m_cp->extractDevices(devices);
  file.close();
}
